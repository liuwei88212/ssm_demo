package com.lw.ssm.util;

import org.apache.ibatis.cache.decorators.LoggingCache;

import com.lw.ssm.common.util.RedisCache;

public class MybatisRedisCache extends LoggingCache {

	public MybatisRedisCache(String id) {
		super(new RedisCache(id));
	}

}
