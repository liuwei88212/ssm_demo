package com.lw.ssm.util;

import java.io.File;
import java.io.FilenameFilter;
import java.util.ArrayList;
import java.util.List;

import javax.swing.filechooser.FileFilter;

class FileFilterByExtension extends FileFilter implements FilenameFilter {

	String suffix;
	
	FileFilterByExtension(String suffix){
		this.suffix = suffix;
	}
	
	public File[] getFiles(String dir){
		
		File file = new File(dir);
		File[] listFiles = file.listFiles();
		
		List<File> list = new ArrayList<>();
		
		for(File f: listFiles){
			if(f.getName().endsWith(suffix) && f.isFile()){
				list.add(f);
			}
		}
		
		return list.toArray(new File[list.size()]);
	}
	
	public boolean accept(File dir, String name) {
		return false;
	}

	public boolean accept(File f) {
		if (f.getName().endsWith(".txt")) {
			return true;
		}
		return false;
	}

	public String getDescription() {
		return null;
	}

}
