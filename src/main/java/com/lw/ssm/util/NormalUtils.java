package com.lw.ssm.util;

import org.apache.commons.math3.distribution.NormalDistribution;

import com.lw.ssm.test.math.normal.NormalDistribution3;

public class NormalUtils {

	public static void main(String[] args) {
		double miu = 170;
		double sigma = 10;
		double x = 149;

		double d1 = normal(x, miu, sigma);
		System.out.println("normal()=" + d1);

		NormalDistribution normalDistributioin = new NormalDistribution(miu,sigma);
		double d2 = normalDistributioin.cumulativeProbability(x);
		System.out.println("cumulativeProbability()=" + d2);

		double d3 = calcNormalDistribution(x);
		System.out.println("calcNormalDistribution()=" + d3);

		double d4 = NORMSDIST(x);
		System.out.println("NORMSDIST()=" + d4);

		double d5 = NormalDistribution3.NORMSDIST(x);
		System.out.println("NormalTest.NORMSDIST()=" + d5);
		
		double d6 = NormalDistribution3.NORMSDIST2(x);
		System.out.println("NormalTest.NORMSDIST2()=" + d6);
	}

	/**
	 * 正态分布公式
	 * 
	 * @param x
	 * @param miu
	 * @param sigma
	 * @return
	 */
	public static double normal(double x, double miu, double sigma) {

		double a = 1 / (Math.sqrt((2 * Math.PI)) * sigma);

		double b = Math.exp((-1) * Math.pow((x - miu), 2) / (2 * sigma * sigma));

		double z = a * b;
		
		return z;
	}

	/**
	 * 标准正态分布算法
	 * 
	 * @param x
	 * @return
	 */
	public static double calcNormalDistribution(double x) {

		double absX = Math.abs(x);
		double x2 = Math.pow(x, 2);// X平方
		double z = Math.exp(-0.5 * x2) * 0.398942280401432678; // 标准正态公式

		double y = 0;

		int k = 28;
		double s = -1;
		double fj = k;

		if (absX > 3) {
			// 当|x|>3时
			for (int i = 1; i <= k; i++) {
				y = fj / (absX + y);
				fj = fj - 1.0;
			}
			y = z / (absX + y);
		} else {
			// 当|x|<3时
			for (int i = 1; i <= k; i++) {
				y = fj * x2 / (2.0 * fj + 1.0 + s * y);
				s = -s;
				fj = fj - 1.0;
			}
			y = 0.5 - z * absX / (1 - y);
		}

		if (x > 0) {
			y = 1.0 - y;
		}

		return y;
	}

	public static double NORMSDIST(double a) {
		double p = 0.2316419;
		double b1 = 0.31938153;
		double b2 = -0.356563782;
		double b3 = 1.781477937;
		double b4 = -1.821255978;
		double b5 = 1.330274429;

		double x = Math.abs(a);
		double t = 1 / (1 + p * x);

		double val = 1
				- (1 / (Math.sqrt(2 * Math.PI)) * Math.exp(-1 * Math.pow(a, 2)
						/ 2))
				* (b1 * t + b2 * Math.pow(t, 2) + b3 * Math.pow(t, 3) + b4
						* Math.pow(t, 4) + b5 * Math.pow(t, 5));

		if (a < 0) {
			val = 1 - val;
		}

		return val;
	}
}
