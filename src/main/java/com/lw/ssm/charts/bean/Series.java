package com.lw.ssm.charts.bean;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.alibaba.fastjson.JSON;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.lw.ssm.charts.util.EchartsUtil;

@JsonInclude(Include.NON_EMPTY)
public class Series {
	public String name;

	public String type;

	public List<Integer> data;// 这里要用int 不能用String 不然前台显示不正常（特别是在做数学运算的时候）
	
	public Integer[][] dataAry;
	
	public Object[][] dataAryObj;
	
	public Series(String name, String type, List<Integer> data) {
		super();
		this.name = name;
		this.type = type;
		this.data = data;
	}
	
	public Series(Integer[][] dataAry) {
		super();
		this.dataAry = dataAry;
	}
	
	public Series(Object[][] dataAry) {
		super();
		this.dataAryObj = dataAry;
	}
	
	
	public static void main(String[] args) throws JsonProcessingException {
		 
		 ArrayList<Integer> arrayList = new ArrayList<Integer>(Arrays.asList(21,23,28,26,21,33,44));
		 
		 String jsonString = JSON.toJSONString(arrayList);
		 System.out.println(jsonString);
		 
		 Integer[][] test = {{10,2},{10,2}};
		 jsonString = JSON.toJSONString(test);
		 System.out.println(jsonString);
		 
		 System.out.println("====================================");
		 
		 
		 List<Series> series = new ArrayList<Series>();//纵坐标  
		 Object[][] data = EchartsUtil.toArray2(EchartsUtil.sumRandomDistribution(170d, 10d));
	     series.add(new Series(data));  
	     EchartData echartData = new EchartData(series);
	     
		 jsonString = JSON.toJSONString(echartData);
		 System.out.println(jsonString);
		 
		 ObjectMapper mapper = new ObjectMapper(); //转换器  
		 jsonString = mapper.writeValueAsString(echartData);
		 System.out.println(jsonString);
	}
}
