package com.lw.ssm.charts.bean;

import java.io.Serializable;

public class MapBean<X,Y> implements Serializable {

	private static final long serialVersionUID = -1539625488671528557L;

	X x;
	
	Y y;
	
	public MapBean(){
	}
	
	public MapBean(X x, Y y){
		this.x = x ;
		this.y = y;
	}

	public X getX() {
		return x;
	}

	public void setX(X x) {
		this.x = x;
	}

	public Y getY() {
		return y;
	}

	public void setY(Y y) {
		this.y = y;
	}
	
	
	
}
