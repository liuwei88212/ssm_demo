package com.lw.ssm.charts.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONArray;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.lw.ssm.charts.bean.EchartData;
import com.lw.ssm.charts.bean.Series;
import com.lw.ssm.charts.util.EchartsUtil;

@Controller
@RequestMapping("/echarts")
public class EchartsController {
	
	Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@ModelAttribute
	public ObjectMapper getJackson(){
		return new ObjectMapper();
	}

	@RequestMapping("/line")
	@ResponseBody
	public EchartData line(){
		 logger.info("lineData....");  
         
        List<String> legend = new ArrayList<String>(Arrays.asList(new String[]{"最高气温"}));//数据分组  
        List<String> category = new ArrayList<String>(Arrays.asList(new String []{"周一","周二","周三","周四","周五","周六","周日"}));//横坐标  
        List<Series> series = new ArrayList<Series>();//纵坐标  
          
        series.add(new Series("最高气温", "line",   new ArrayList<Integer>(Arrays.asList(21,23,28,26,21,33,44))));  
          
        EchartData data = new EchartData(legend, category, series);
        
        try {
       	 logger.info("--->{}", getJackson().writeValueAsString(data));
		} catch (Exception e) {
		}
        
        return data;  
	}
	
	
	@RequestMapping("/scatter")
	@ResponseBody
	public EchartData scatter(){
		 logger.info("scatterData....");  
         
		 //散列点
        List<Series> series = new ArrayList<Series>();
          
        Object[][] data = EchartsUtil.toArray2(EchartsUtil.sumRandomDistribution(170d, 10d));
        series.add(new Series(data));  
        
        EchartData echartData = new EchartData(series);
        
        try {
        	 logger.info("--->{}", getJackson().writeValueAsString(echartData));
		} catch (Exception e) {
		}
       
        return echartData;  
	}
	
	
	
	
	
	public static void main(String[] args) {
		
		String[] array1 = new String[2];
		array1[0]="a1";
		array1[1]="b1";
		
		String[] array2 = new String[2];
		array2[0]="a2";
		array2[1]="b2";
		
		String[][] array = new String[2][2];
		array[0]=array1;
		array[1]=array2;
		
		String jsonString = JSONArray.toJSONString(array);
		System.out.println(jsonString);
	}
	
}
