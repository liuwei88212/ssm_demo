package com.lw.ssm.charts.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.lw.ssm.charts.bean.MapBean;

/**
 * Echart工具类
 * 
 * Javascript 随机数函数 学习之一：产生服从均匀分布随机数： http://www.cnblogs.com/zztt/p/4024906.html
 * 
 * @author Administrator
 *
 */
public class EchartsUtil {
	
	/**
	 * 生成正态随机数
	 * 
	 * @param mean
	 * @param std_dev
	 * @return
	 */
	private static double getNumberInNormalDistribution2(double mean, double std_dev){
		return mean + (uniform2NormalDistribution() * std_dev);
		
//		return mean + (randomNormalDistribution() * std_dev);
	}
	
	@SuppressWarnings("unused")
	private static double uniform2NormalDistribution(){
		double sum = 0.0;
		
		for(int i=0;i< 12;i++){
			 sum =sum + Math.random();
		}
		return sum-6.0;
	}
	
	private static double randomNormalDistribution(){
		double u=0.0, v=0.0, w=0.0, c=0.0;
	    do{
	        //获得两个（-1,1）的独立随机变量
	        u = Math.random()*2-1.0;
	        v = Math.random()*2-1.0;
	        
	        w = u*u + v*v;
	        
	    }while(w==0.0||w>=1.0);
	    
	    //这里就是 Box-Muller转换
	    c = Math.sqrt((-2*Math.log(w))/w);
	    
	    //返回2个标准正态分布的随机数，封装进一个数组返回
	    //当然，因为这个函数运行较快，也可以扔掉一个
	    //return [u*c,v*c];
	    return u*c;
	}
	
	/**
	 * 统计正态数数量
	 * @return
	 */
	public static Map<Integer,Double> sumRandomDistribution(double mean, double std_dev){
		double miu = mean;
		double sigma = std_dev;
		
		Map<Integer,Integer> map = new HashMap<Integer, Integer>();
		int total = 1000;
		for (int i = 0; i < total; i++) {
			Double floor = Math.floor(getNumberInNormalDistribution2(miu, sigma));
			Integer key = floor.intValue();
			
			if(map.get(key) == null){
				map.put(key, 1);
			}else{
				map.put(key, map.get(key)+1);
			}
		}
		
		Map<Integer, Double> percentMap = new HashMap<>();
		Iterator<Entry<Integer, Integer>> iterator = map.entrySet().iterator();
		while (iterator.hasNext()) {
			Entry<Integer, Integer> next = iterator.next();
			Double value = new Double(next.getValue());
			
			//System.out.println(next.getKey()+":"+next.getValue());
			percentMap.put(next.getKey(), (value/total));
		}
		return percentMap;
	}
	
	public static Object[][] toArray2(Map<Integer, Double> map){
		Iterator<Entry<Integer, Double>> iterator = map.entrySet().iterator();
		Object[][] ary = new Object[map.size()][2];
		int i=0;
		while (iterator.hasNext()) {
			Entry<Integer, Double> next = iterator.next();
			
			Object[] a = new Object[2];
			a[0] = next.getKey();
			a[1] = next.getValue();
			
			ary[i] = a;
			i++;
		}
		
		return ary;
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static List<MapBean> mapToList(Map<Integer, Double> map){
		
		Iterator<Entry<Integer, Double>> iterator = map.entrySet().iterator();
		
		List<MapBean> list = new ArrayList<MapBean>();
		while (iterator.hasNext()) {
			Entry<Integer, Double> next = iterator.next();
			
			Integer key = next.getKey();
			Double value = next.getValue();
			
			MapBean mapBean = new MapBean(key, value);
			
			list.add(mapBean);
		}
		
		return list;
	}
	
	
	public static void main(String[] args) throws JsonProcessingException {
		Map<Integer, Double> randomDistributionMap = sumRandomDistribution(170, 10);
		
//		Integer[][] array2 = toArray2(randomDistributionMap);
		
		List<MapBean> mapToList = mapToList(randomDistributionMap);
		
		ObjectMapper objMapper = new ObjectMapper();
		String writeValueAsString = objMapper.writeValueAsString(mapToList);
		
		System.out.println(writeValueAsString);
//		
//		Iterator<Entry<Integer, Integer>> iterator = randomDistributionMap.entrySet().iterator();
//		Integer[][] ary = new Integer[randomDistributionMap.size()][2];
//		int i=0;
//		while (iterator.hasNext()) {
//			Entry<Integer, Integer> next = iterator.next();
//			System.out.println("Key = " + next.getKey() + ", Value = " + next.getValue()); 
//			
//			Integer[] a = new Integer[2];
//			a[0] = next.getKey();
//			a[1] = next.getValue();
//			
//			ary[i] = a;
//			i++;
//		}
		
//		MapBean<Integer, Integer> mapBean = new MapBean<Integer, Integer>(1,2);	
//		
//		MapBean<String, String> mapBean2 = new MapBean<String, String>("11111111","2"); 
//		
//		System.out.println(mapBean.getX());
//		
//		System.out.println(mapBean2.getX());
	}
}
