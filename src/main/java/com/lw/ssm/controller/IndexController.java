package com.lw.ssm.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/")
public class IndexController {
	
	Logger logger = LoggerFactory.getLogger(this.getClass());
	
	
	@RequestMapping("echarts")
	public String charts(){
		return "echarts/index";
	}
	
}
