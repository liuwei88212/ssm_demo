package com.lw.ssm.common.util;

import java.util.List;
import java.util.Map;

import org.apache.commons.configuration.CompositeConfiguration;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

import com.lw.ssm.util.StringUtils;

/**
 * Redis 工具类
 * 
 * 参考1：http://www.open-open.com/code/view/1430406110599
 * 
 * @author Administrator
 * 
 */
public class BinaryRedisUtil {
	protected static Logger logger = LoggerFactory
			.getLogger(BinaryRedisUtil.class);

	private static JedisPool jedisPool = null;

	/** redis过期时间,以秒为单位 */
	public final static int EXRP_HOUR = 60 * 60; // 一小时
	public final static int EXRP_DAY = 60 * 60 * 24; // 一天
	public final static int EXRP_MONTH = 60 * 60 * 24 * 30; // 一个月

	private static String host;
	private static int port;
	private static String pwd;

	private static int maxIdle;
	private static int maxActive;
	private static int maxWait;
	private static int timeout;
	private static boolean testOnBorrow;

	static {
		CompositeConfiguration config = new CompositeConfiguration();
		try {
			config.addConfiguration(new PropertiesConfiguration(
					"conf/redis.properties"));

			// Properties config2 = new Properties();
			// config2.load(RedisUtil.class.getClassLoader().getResourceAsStream("config/redis.properties"));
		} catch (ConfigurationException e) {
			e.printStackTrace();
		}
		// 从配置文件中获取属性值
		host = config.getString("redis.host");
		port = config.getInt("redis.port");
		pwd = config.getString("redis.pwd");

		maxIdle = config.getInt("redis.maxIdle");
		maxActive = config.getInt("redis.maxActive");
		maxWait = config.getInt("redis.maxWait");
		timeout = config.getInt("redis.timeout");
		testOnBorrow = config.getBoolean("redis.testOnBorrow");
	}

	/**
	 * 初始化Redis连接池
	 */
	private static void initialPool() {
		try {
			JedisPoolConfig config = new JedisPoolConfig();
			config.setMaxIdle(maxIdle);
			config.setMaxTotal(maxActive);
			config.setMaxWaitMillis(maxWait);
			config.setTestOnBorrow(testOnBorrow);

			if (!StringUtils.isEmpty(pwd)) {
				jedisPool = new JedisPool(config, host, port, timeout, pwd);
			} else {
				jedisPool = new JedisPool(config, host, port, timeout);
			}

		} catch (Exception e) {
			logger.error("First create JedisPool error : " + e);
		}
	}

	/**
	 * 在多线程环境同步初始化
	 */
	private static synchronized void poolInit() {
		if (jedisPool == null) {
			initialPool();
		}
	}

	/**
	 * 同步获取Jedis实例
	 * 
	 * @return Jedis
	 */
	public synchronized static Jedis getJedis() {
		if (jedisPool == null) {
			poolInit();
		}
		Jedis jedis = null;
		try {
			if (jedisPool != null) {
				jedis = jedisPool.getResource();
				// if (!StringUtils.isEmpty(pwd)) {
				// jedis.auth(pwd);
				// }

				// jedis.select(1);// 选择Redis数据库
			}
		} catch (Exception e) {
			logger.error("Get jedis error : " + e);
		}
		return jedis;
	}

	/**
	 * 释放jedis资源
	 * 
	 * @param jedis
	 */
	public static void close(final Jedis jedis) {
		// if (jedis != null && jedisPool != null) {
		// jedisPool.returnResource(jedis);
		// }

		if (jedis != null) {
			jedis.close();
		}
	}

	public static void del(byte[] key) {
		Jedis jedis = null;
		try {
			// logger.debug("--->del: {}", byte2Obj(key));

			jedis = getJedis();
			jedis.del(key);

		} catch (Exception e) {
			logger.error("del key error : " + e);
		} finally {
			if (jedis != null) {
				close(jedis);
			}
		}
	}

	public static void set(byte[] key, byte[] value, int time) {
		Jedis jedis = null;
		try {
			// logger.debug("--->set: {}, {}", byte2Obj(key), byte2Obj(value));

			jedis = getJedis();
			jedis.set(key, value);

			if (time != 0) {
				jedis.expire(key, time);
			}

		} catch (Exception e) {
			logger.error("Set key error : " + e);
		} finally {
			if (jedis != null) {
				close(jedis);
			}
		}
	}

	public static byte[] get(byte[] key) {
		Jedis jedis = getJedis();
		if (jedis == null || !jedis.exists(key)) {
			return null;
		}
		try {
			// logger.debug("--->get: {}", byte2Obj(key));

			jedis = getJedis();
			return jedis.get(key);
		} catch (Exception e) {
			logger.error("Get key error : " + e);
		} finally {
			close(jedis);
		}
		return null;
	}

	public static Long expire(byte[] key, int seconds) {
		Jedis jedis = getJedis();
		if (jedis == null || !jedis.exists(key)) {
			return null;
		}
		try {
			// logger.debug("--->expire: {}, time: {}", byte2Obj(key), seconds);

			return jedis.expire(key, seconds);
		} catch (Exception e) {
			logger.error("get keyex error : " + e);
		} finally {
			close(jedis);
		}
		return null;
	}

	/**
	 * 将哈希表 key 中的域 field 的值设为 value 。 如果 key 不存在，一个新的哈希表被创建并进行HSET 操作。 如果域
	 * field 已经存在于哈希表中，旧值将被覆盖
	 * 
	 * @param key
	 * @param field
	 * @param value
	 */
	public static void hset(byte[] key, byte[] field, byte[] value) {
		Jedis jedis = null;
		try {
			// logger.debug("--->hset: {}, {}, {}", byte2Obj(key),
			// byte2Obj(field), byte2Obj(value));

			jedis = getJedis();
			jedis.hset(key, field, value);
		} catch (Exception e) {
			logger.error("hset key error : " + e);
		} finally {
			// 返还到连接池
			close(jedis);
		}
	}

	/**
	 * 返回哈希表 key 中给定域 field 的值.
	 * 
	 * @param key
	 * @param field
	 */
	public static void hget(byte[] key, byte[] field) {
		Jedis jedis = null;
		try {
			// logger.debug("--->hget: {}, {}", byte2Obj(key), byte2Obj(field));

			jedis = getJedis();
			jedis.hget(key, field);
		} catch (Exception e) {
			logger.error("hget key error : " + e);
		} finally {
			// 返还到连接池
			close(jedis);
		}
	}

	/**
	 * 返回哈希表 key 中，所有的域和值
	 * 
	 * @param key
	 * @return
	 */
	public static Map<byte[], byte[]> hgetAll(byte[] key) {
		Map<byte[], byte[]> list = null;
		Jedis jedis = null;
		try {
			// logger.debug("--->hgetAll: {}", byte2Obj(key));

			jedis = getJedis();
			list = jedis.hgetAll(key);
		} catch (Exception e) {
			logger.error("hgetAll key error : " + e);
		} finally {
			// 返还到连接池
			close(jedis);
		}
		return list;
	}

	public static void hdel(byte[] key, byte[] field) {
		Jedis jedis = null;
		try {
			// logger.debug("--->hdel: {}, {}", byte2Obj(key), byte2Obj(field));

			jedis = getJedis();
			jedis.hdel(key, field);
		} catch (Exception e) {
			logger.error("hdel key error : " + e);
		} finally {
			// 返还到连接池
			close(jedis);
		}
	}

	/**
	 * 存储REDIS队列 顺序存储
	 * 
	 * @param byte[] key reids键名
	 * @param byte[] value 键值
	 */
	public static void lpush(byte[] key, byte[] value) {
		Jedis jedis = null;
		try {
			// logger.debug("--->lpush: {}, {}", byte2Obj(key),
			// byte2Obj(value));

			jedis = getJedis();
			jedis.lpush(key, value);
		} catch (Exception e) {
			logger.error("lpush key error : " + e);
		} finally {
			// 返还到连接池
			close(jedis);
		}
	}

	/**
	 * 存储REDIS队列 反向存储
	 * 
	 * @param byte[] key reids键名
	 * @param byte[] value 键值
	 */
	public static void rpush(byte[] key, byte[] value) {
		Jedis jedis = null;
		try {
			// logger.debug("--->rpush: {}, {}", byte2Obj(key),
			// byte2Obj(value));

			jedis = getJedis();
			jedis.rpush(key, value);
		} catch (Exception e) {
			logger.error("rpush key error : " + e);
		} finally {
			// 返还到连接池
			close(jedis);
		}
	}

	/**
	 * BRPOPLPUSH 是RPOPLPUSH 的阻塞版本，当给定列表 source 不为空时，BRPOPLPUSH 的表现 和RPOPLPUSH一样
	 * 
	 * 当列表 source 为空时，BRPOPLPUSH 命令将阻塞连接，直到等待超时，或有另一个客户端对 source 执行LPUSH 或RPUSH
	 * 命令为止。
	 * 
	 * @param byte[] key reids键名
	 * @param byte[] value 键值
	 */
	public static byte[] brpoplpush(byte[] key, byte[] destination) {

		byte[] value = null;
		Jedis jedis = null;
		try {
			// logger.debug("--->rpoplpush: {}, {}", byte2Obj(key),
			// byte2Obj(destination));

			jedis = getJedis();
			value = jedis.brpoplpush(key, destination, 0);
		} catch (Exception e) {
			logger.error("rpoplpush key error : " + e);
		} finally {
			// 返还到连接池
			close(jedis);
		}

		return value;
	}

	/**
	 * 获取队列数据
	 * 
	 * @param byte[] key 键名
	 * @return
	 */
	public static List<byte[]> lpopList(byte[] key) {

		List<byte[]> list = null;
		Jedis jedis = null;
		try {
			// logger.debug("--->lpopList: {}", byte2Obj(key));

			jedis = getJedis();
			list = jedis.lrange(key, 0, -1);
		} catch (Exception e) {
			logger.error("lpopList key error : " + e);
		} finally {
			// 返还到连接池
			close(jedis);
		}

		return list;
	}

	/**
	 * 获取队列某条数据
	 * 
	 * @param byte[] key 键名
	 * @return
	 */
	public static byte[] rpop(byte[] key) {

		byte[] value = null;
		Jedis jedis = null;
		try {
			// logger.debug("--->rpop: {}", byte2Obj(key));

			jedis = getJedis();
			value = jedis.rpop(key);
		} catch (Exception e) {
			logger.error("rpop key error : " + e);
		} finally {
			System.out.println("----------rpop close()----------");
			// 返还到连接池
			close(jedis);
		}
		return value;
	}

	/**
	 * 它是RPOP 命令的阻塞版本，当给定列表内没有任何元素可供弹出的时候，连接将被BRPOP 命令阻塞，直到等待超时或发现可弹出元素为止。
	 * 
	 * @param key
	 * @param time
	 * @return
	 */
	public static List<byte[]> brpop(byte[] key, int time) {
		List<byte[]> value = null;
		Jedis jedis = null;
		try {
			jedis = getJedis();
			value = jedis.brpop(time, key); // 0无限长
		} catch (Exception e) {
			logger.error("brpop key error : " + e);
		} finally {
			System.out.println("----------brpop close()----------");
			// 返还到连接池
			close(jedis);
		}
		return value;
	}

	/**
	 * 它是LPOP 命令的阻塞版本，当给定列表内没有任何元素可供弹出的时候，连接将被BLPOP 命令阻塞，直到等待超时或发现可弹出元素为止
	 * 
	 * @param key
	 * @param time
	 * @return
	 */
	public static List<byte[]> blpop(byte[] key, int time) {
		List<byte[]> value = null;
		Jedis jedis = null;
		try {
			jedis = getJedis();
			value = jedis.blpop(time, key); // 0无限长
		} catch (Exception e) {
			logger.error("blpop key error : " + e);
		} finally {
			// 返还到连接池
			close(jedis);
		}
		return value;
	}

	/**
	 * 获取队列某条数据
	 * 
	 * @param byte[] key 键名
	 * @return
	 */
	public static void hmset(byte[] key, Map<byte[], byte[]> hash) {

		Jedis jedis = null;
		try {
			// logger.debug("--->hmset: {}", byte2Obj(key));

			jedis = getJedis();
			jedis.hmset(key, hash);
		} catch (Exception e) {
			logger.error("hmset key error : " + e);
		} finally {
			// 返还到连接池
			close(jedis);
		}
	}

	/**
	 * HMGET key field [field ...] 返回哈希表 key 中，一个或多个给定域的值。 如果给定的域不存在于哈希表，那么返回一个
	 * nil 值
	 * 
	 * @param byte[] key 键名
	 * @return
	 */
	public static List<byte[]> hmget(byte[] key, byte[]... fields) {
		List<byte[]> list = null;
		Jedis jedis = null;
		try {
			// logger.debug("--->hmset: {}", byte2Obj(key));

			jedis = getJedis();
			list = jedis.hmget(key, fields);
		} catch (Exception e) {
			logger.error("hmset key error : " + e);
		} finally {
			// 返还到连接池
			close(jedis);
		}

		return list;
	}

	public static List<byte[]> lrange(byte[] key, int start, int end) {
		List<byte[]> list = null;
		Jedis jedis = null;
		try {
			// logger.debug("--->lrange: {}, {}, {}", byte2Obj(key), start,
			// end);

			jedis = getJedis();
			list = jedis.lrange(key, start, end);
		} catch (Exception e) {
			logger.error("lrange key error : " + e);
		} finally {
			// 返还到连接池
			close(jedis);
		}

		return list;
	}

	/**
	 * 返回列表 key 的长度。 如果 key 不存在，则 key 被解释为一个空列表，返回 0. 如果 key 不是列表类型，返回一个错误。
	 * 
	 * @param key
	 * @return
	 */
	public static long llen(byte[] key) {
		long len = 0;
		Jedis jedis = null;
		try {
			// logger.debug("--->llen: {}", byte2Obj(key));

			jedis = getJedis();
			len = jedis.llen(key);
		} catch (Exception e) {
			logger.error("llen key error : " + e);
		} finally {
			// 返还到连接池
			close(jedis);
		}
		return len;
	}

	@SuppressWarnings("unused")
	private static Object byte2Obj(byte[] bytes) {
		return SerializeUtil.unserialize(bytes);

	}
}
