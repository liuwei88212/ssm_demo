package com.lw.ssm.common.util;

import java.util.List;
import java.util.Map;

import org.apache.commons.configuration.CompositeConfiguration;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.lw.ssm.util.StringUtils;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

/**
 * Redis 工具类
 * 
 * 参考1：http://www.open-open.com/code/view/1430406110599
 * 
 * @author Administrator
 * 
 */
public class RedisUtil {
	protected static Logger logger = LoggerFactory.getLogger(RedisUtil.class);

	private static JedisPool jedisPool = null;

	/** redis过期时间,以秒为单位 */
	public final static int EXRP_HOUR = 60 * 60; // 一小时
	public final static int EXRP_DAY = 60 * 60 * 24; // 一天
	public final static int EXRP_MONTH = 60 * 60 * 24 * 30; // 一个月

	private static String host;
	private static int port;
	private static String pwd;

	private static int maxIdle;
	private static int maxActive;
	private static int maxWait;
	private static int timeout;
	private static boolean testOnBorrow;

	static {
		CompositeConfiguration config = new CompositeConfiguration();
		try {
			config.addConfiguration(new PropertiesConfiguration(
					"conf/redis.properties"));

			// Properties config2 = new Properties();
			// config2.load(RedisUtil.class.getClassLoader().getResourceAsStream("config/redis.properties"));
		} catch (ConfigurationException e) {
			e.printStackTrace();
		}
		// 从配置文件中获取属性值
		host = config.getString("redis.host");
		port = config.getInt("redis.port");
		pwd = config.getString("redis.pwd");

		maxIdle = config.getInt("redis.maxIdle");
		maxActive = config.getInt("redis.maxActive");
		maxWait = config.getInt("redis.maxWait");
		timeout = config.getInt("redis.timeout");
		testOnBorrow = config.getBoolean("redis.testOnBorrow");
	}

	/**
	 * 初始化Redis连接池
	 */
	private static void initialPool() {
		try {
			JedisPoolConfig config = new JedisPoolConfig();
			config.setMaxIdle(maxIdle);
			config.setMaxTotal(maxActive);
			config.setMaxWaitMillis(maxWait);
			config.setTestOnBorrow(testOnBorrow);

			if (!StringUtils.isEmpty(pwd)) {
				jedisPool = new JedisPool(config, host, port, timeout, pwd);
			} else {
				jedisPool = new JedisPool(config, host, port, timeout);
			}

		} catch (Exception e) {
			logger.error("First create JedisPool error : " + e);
		}
	}

	/**
	 * 在多线程环境同步初始化
	 */
	private static synchronized void poolInit() {
		if (jedisPool == null) {
			initialPool();
		}
	}

	/**
	 * 同步获取Jedis实例
	 * 
	 * @return Jedis
	 */
	public synchronized static Jedis getJedis() {
		if (jedisPool == null) {
			poolInit();
		}
		Jedis jedis = null;
		try {
			if (jedisPool != null) {
				jedis = jedisPool.getResource();
				// if (!StringUtils.isEmpty(pwd)) {
				// jedis.auth(pwd);
				// }

				// jedis.select(1);// 选择Redis数据库
			}
		} catch (Exception e) {
			logger.error("Get jedis error : " + e);
		}
		return jedis;
	}

	/**
	 * 释放jedis资源
	 * 
	 * @param jedis
	 */
	public static void close(final Jedis jedis) {
		// if (jedis != null && jedisPool != null) {
		// jedisPool.returnResource(jedis);
		// }

		if (jedis != null) {
			jedis.close();
		}
	}

	public static void del(String key) {
		Jedis jedis = getJedis();
		if (jedis.exists(key)) {
			jedis.del(key);
		}
	}

	/**
	 * 设置 String
	 * 
	 * @param key
	 * @param value
	 */
	public static void set(byte[] key, byte[] value) {
		Jedis jedis = null;
		try {

			logger.debug("======>" + key + ":" + value);
			jedis = getJedis();
			jedis.set(key, value);

		} catch (Exception e) {
			logger.error("Set key error : " + e);
		} finally {
			if (jedis != null) {
				close(jedis);
			}
		}
	}

	public static byte[] get(byte[] key) {
		Jedis jedis = getJedis();
		if (jedis == null || !jedis.exists(key)) {
			return null;
		}
		try {
			jedis = getJedis();
			return jedis.get(key);
		} catch (Exception e) {
			logger.error("get keyex error : " + e);
		} finally {
			close(jedis);
		}
		return null;
	}

	/**
	 * 设置 String
	 * 
	 * @param key
	 * @param value
	 */
	public static void setString(String key, String value) {
		try {
			value = StringUtils.isEmpty(value) ? "" : value;
			getJedis().set(key, value);
		} catch (Exception e) {
			logger.error("Set key error : " + e);
		}
	}

	/**
	 * 设置 过期时间
	 * 
	 * @param key
	 * @param seconds
	 *            以秒为单位
	 * @param value
	 */
	public static void setEx(String key, String value, int seconds) {
		Jedis jedis = null;

		try {
			value = StringUtils.isEmpty(value) ? "" : value;

			jedis = getJedis();
			jedis.setex(key, seconds, value);

		} catch (Exception e) {
			logger.error("Set keyex error : " + e);
		} finally {
			close(jedis);
		}
	}

	/**
	 * 获取String值
	 * 
	 * @param key
	 * @return value
	 */
	public static String get(String key) {
		Jedis jedis = getJedis();

		if (jedis == null || !jedis.exists(key)) {
			return null;
		}
		try {
			jedis = getJedis();
			return jedis.get(key);

		} catch (Exception e) {
			logger.error("get keyex error : " + e);
		} finally {
			close(jedis);
		}
		return null;
	}

	public static Long expire(byte[] key, int seconds) {
		Jedis jedis = getJedis();
		if (jedis == null || !jedis.exists(key)) {
			return null;
		}
		try {
			return jedis.expire(key, seconds);
		} catch (Exception e) {
			logger.error("get keyex error : " + e);
		} finally {
			close(jedis);
		}
		return null;
	}

	/**
	 * 获取lrange缓存
	 * 
	 * @param key
	 *            键
	 * @return 值
	 */
	public static List<String> lrange(String key) {
		List<String> value = null;
		Jedis jedis = null;
		try {
			jedis = getJedis();
			if (jedis.exists(key)) {
				value = jedis.lrange(key, 0, -1);
				logger.debug("lrange {} = {}", key, value);
			}
		} catch (Exception e) {
			logger.error("lrange {} = {}", key, value, e);
		} finally {
			close(jedis);
		}
		return value;
	}

	public static long llen(String key) {
		long len = 0;
		Jedis jedis = null;
		try {
			jedis = getJedis();
			if (jedis.exists(key)) {
				len = jedis.llen(key);
				logger.debug("llen {} ", key);
			}
		} catch (Exception e) {
			logger.error("lrange {} ", key);
		} finally {
			close(jedis);
		}
		return len;
	}

	/**
	 * 设置rpush缓存
	 * 
	 * @param key
	 *            键
	 * @param value
	 *            值
	 * @param cacheSeconds
	 *            超时时间，0为不超时
	 * @return
	 */
	public static long rpush(String key, List<String> list, int cacheSeconds) {
		long result = 0;
		Jedis jedis = null;
		String[] o = null;
		try {
			jedis = getJedis();

			o = new String[list.size()];
			list.toArray(o);

			result = jedis.rpush(key, o);
			if (cacheSeconds != 0) {
				jedis.expire(key, cacheSeconds);
			}
			//logger.debug("rpush {} = {}", key, o);

		} catch (Exception e) {
			logger.error("rpush {} = {}", key, o, e);
		} finally {
			close(jedis);
		}
		return result;
	}

	/**
	 * 设置rpush缓存
	 * 
	 * @param key
	 *            键
	 * @param value
	 *            值
	 * @param cacheSeconds
	 *            超时时间，0为不超时
	 * @return
	 */
	public static long lpush(String key, List<String> list, int cacheSeconds) {
		long result = 0;
		Jedis jedis = null;
		String[] o = null;
		try {
			jedis = getJedis();

			o = new String[list.size()];
			list.toArray(o);

			result = jedis.lpush(key, o);
			if (cacheSeconds != 0) {
				jedis.expire(key, cacheSeconds);
			}
			logger.debug("lpush {} = {}", key, o);

		} catch (Exception e) {
			logger.error("lpush {} = {}", key, o, e);
		} finally {
			close(jedis);
		}
		return result;
	}

	/**
	 * 将列表 source 中的最后一个元素(尾元素)弹出，并返回给客户端
	 * 
	 * @param byte[] key reids键名
	 * @param byte[] value 键值
	 */
	public static void rpoplpush(String key, String destination) {

		Jedis jedis = null;
		try {

			jedis = getJedis();
			jedis.rpoplpush(key, destination);

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			// 返还到连接池
			close(jedis);

		}
	}

	/**
	 * zadd：
	 * 
	 * 将一个或多个 member 元素及其 score 值加入到有序集 key 当中。如果某个 member 已经是有序集的成员， 那么更新这个
	 * member 的 score 值，并通过重新插入这个 member元素，来保证该 member 在正确的位置上。
	 * 
	 * @param key
	 *            键
	 * @param value
	 *            值
	 * @param cacheSeconds
	 *            超时时间，0为不超时
	 * @return 数量
	 */
	public static long zadd(String key, Map<String, Double> scoreMembers,
			int cacheSeconds) {
		long result = 0;
		Jedis jedis = null;
		try {
			jedis = getJedis();
			result = jedis.zadd(key, scoreMembers);
			if (cacheSeconds != 0) {
				jedis.expire(key, cacheSeconds);
			}
			logger.debug("zadd: {} = {}", key, scoreMembers);

		} catch (Exception e) {
			logger.error("zadd: {} = {}", key, e);
		} finally {
			close(jedis);
		}
		return result;
	}

	/**
	 * 
	 * sadd：
	 * 
	 * 将一个或多个 member 元素加入到集合 key 当中，已经存在于集合的 member 元素将被忽略。 假如 key 不存在，则创建一个只包含
	 * member 元素作成员的集合。
	 * 
	 * @param key
	 *            键
	 * @param value
	 *            值
	 * @param cacheSeconds
	 *            超时时间，0为不超时
	 * @return 数量
	 */
	public static long sadd(String key, List<String> list, int cacheSeconds) {
		long result = 0;
		Jedis jedis = null;
		String[] o = null;
		try {
			jedis = getJedis();

			o = new String[list.size()];
			list.toArray(o);

			result = jedis.sadd(key, o);
			if (cacheSeconds != 0) {
				jedis.expire(key, cacheSeconds);
			}
			logger.debug("sadd: {} = {}", key, o);

		} catch (Exception e) {
			logger.error("sadd: {} = {}", key, e);
		} finally {
			close(jedis);
		}
		return result;
	}

	/**
	 * hmSet：
	 * 
	 * 同时将多个 field-value (域 -值) 对设置到哈希表 key 中。此命令会覆盖哈希表中已存在的域
	 * 
	 * @param key
	 *            键
	 * @param value
	 *            值
	 * @param cacheSeconds
	 *            超时时间，0为不超时
	 * @return 状态
	 */
	public static String hmset(String key, Map<String, String> map,
			int cacheSeconds) {
		String result = null;
		Jedis jedis = null;
		try {
			jedis = getJedis();
			result = jedis.hmset(key, map);
			if (cacheSeconds != 0) {
				jedis.expire(key, cacheSeconds);
			}
			logger.debug("hmset: {} = {}", key, map);

		} catch (Exception e) {
			logger.error("hmset: {} = {}", key, e);
		} finally {
			close(jedis);
		}
		return result;
	}
}
