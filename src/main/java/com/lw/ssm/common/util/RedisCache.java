package com.lw.ssm.common.util;

import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.ibatis.cache.Cache;

import redis.clients.jedis.Jedis;

/**
 * 
 * 
 * 类名称：RedisCache 类描述：使用第三方缓存服务器redis，处理二级缓存 修改备注：
 * 
 * @version V1.0
 * 
 */
public class RedisCache implements Cache {
	private static Log log = LogFactory.getLog(RedisCache.class);

	/** The ReadWriteLock. */
	private final ReadWriteLock readWriteLock = new ReentrantReadWriteLock();

	private String id;

	public RedisCache(final String id) {
		if (id == null) {
			throw new IllegalArgumentException("必须传入ID");
		}
		log.debug("------------->MybatisRedisCache: id = " + id);
		this.id = id;
	}

	public String getId() {
		return id;
	}

	public int getSize() {
		Jedis jedis = null;
		int result = 0;
		try {
			jedis = RedisUtil.getJedis();
			result = Integer.valueOf(jedis.dbSize().toString());
		} catch (Exception e) {
			log.error("getSize: ", e);
		} finally {
			jedis.close();
		}
		return result;
	}

	public void putObject(Object key, Object value) {
		if (log.isDebugEnabled())
			log.debug("putObject:" + key.hashCode() + "=" + value);
		if (log.isInfoEnabled())
			log.info("put to redis sql :" + key.toString());

		RedisUtil.set(SerializeUtil.serialize(key.hashCode()),SerializeUtil.serialize(value));
		
//		RedisUtil.setString(String.valueOf(key.hashCode()), JSON.toJSONString(value));
	}

	public Object getObject(Object key) {
		Object value = SerializeUtil.unserialize(RedisUtil.get(SerializeUtil.serialize(key.hashCode())));
//		Object value = RedisUtil.get(String.valueOf(key.hashCode()));
		
		
		if (log.isDebugEnabled())
			log.debug("getObject:" + key.hashCode() + "=" + value);
		return value;
	}

	public Object removeObject(Object key) {
		return RedisUtil.expire(SerializeUtil.serialize(key.hashCode()), 0);
	}

	public void clear() {
		Jedis jedis = null;
		try {
			jedis = RedisUtil.getJedis();
			jedis.flushDB();
			jedis.flushAll();
		} catch (Exception e) {
			log.error("clear:{}", e);
		} finally {
			jedis.close();
		}
	}

	public ReadWriteLock getReadWriteLock() {
		return readWriteLock;
	}

}
