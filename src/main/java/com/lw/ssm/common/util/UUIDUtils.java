package com.lw.ssm.common.util;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

/**
 * 短8位UUID思想其实借鉴微博短域名的生成方式，但是其重复概率过高，而且每次生成4个，需要随即选取一个。
 * 
 * 本算法利用62个可打印字符，通过随机生成32位UUID，由于UUID都为十六进制，所以将UUID分成8组，每4个为一组，然后通过模62操作，结果作为索引取出字符，
 * 
 * Created by liyd on 9/10/14.
 */
public class UUIDUtils {

	public static String[] allChars = new String[] { "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m",
			"n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z", "0", "1", "2", "3", "4", "5", "6", "7",
			"8", "9", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S",
			"T", "U", "V", "W", "X", "Y", "Z" };

	// public static String getOrderNo() {
	// String orderNo = "";
	// UUID uuid = UUID.randomUUID();
	// String trandNo = String.valueOf((Math.random() * 9 + 1) * 1000000);
	// String sdf = new SimpleDateFormat("yyyyMMddHHMMSS").format(new Date());
	// orderNo = uuid.toString().substring(0, 8);
	// orderNo = orderNo + sdf;
	// return orderNo;
	// }

	// 生成19随机单号 纯数字
	public static String getOrderNo() {
		String orderNo = "";
		String trandNo = String.valueOf((Math.random() * 9 + 1) * 1000000);
		String sdf = new SimpleDateFormat("yyyyMMddHHMMSS").format(new Date());
		orderNo = trandNo.toString().substring(0, 4);
		orderNo = orderNo + sdf;
		return orderNo;
	}

	/**
	 * 生成指定长度的uuid
	 * 
	 * @param length
	 * @return
	 */
	private static String getUUID(int length, UUID uuid) {
		int groupLength = 32 / length;
		StringBuilder sb = new StringBuilder();
		String id = uuid.toString().replace("-", "");
		for (int i = 0; i < length; i++) {
			String str = id.substring(i * groupLength, i * groupLength + groupLength);
			int x = Integer.parseInt(str, 16);
			sb.append(allChars[x % 0x3E]);
		}
		return sb.toString();
	}

	/**
	 * 8位UUID
	 * 
	 * @return
	 */
	public static String getUUID8() {
		return getUUID(8, UUID.randomUUID());
	}

	/**
	 * 8位UUID
	 * 
	 * @return
	 */
	public static String getUUID8(byte[] bytes) {
		return getUUID(8, UUID.nameUUIDFromBytes(bytes));
	}

	/**
	 * 8位UUID
	 * 
	 * @return
	 */
	public static String getUUID8(String fromString) {
		return getUUID(8, UUID.fromString(fromString));
	}

	/**
	 * 16位UUID
	 * 
	 * @return
	 */
	public static String getUUID16() {
		return getUUID(16, UUID.randomUUID());
	}

	/**
	 * 16位UUID
	 * 
	 * @return
	 */
	public static String getUUID16(String fromString) {
		return getUUID(16, UUID.fromString(fromString));
	}

	/**
	 * 16位UUID
	 * 
	 * @return
	 */
	public static String getUUID16(byte[] bytes) {
		return getUUID(16, UUID.nameUUIDFromBytes(bytes));
	}

	/**
	 * 32位UUID
	 * 
	 * @return
	 */
	public static String getUUID32() {
		return UUID.randomUUID().toString().replace("-", "");
	}

	public static void main(String[] args) {
		System.out.println(getUUID16());
		System.out.println(getOrderNo());
	}

}
