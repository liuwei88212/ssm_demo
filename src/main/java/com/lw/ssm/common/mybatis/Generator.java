package com.lw.ssm.common.mybatis;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.mybatis.generator.api.MyBatisGenerator;
import org.mybatis.generator.config.Configuration;
import org.mybatis.generator.config.xml.ConfigurationParser;
import org.mybatis.generator.exception.InvalidConfigurationException;
import org.mybatis.generator.exception.XMLParserException;
import org.mybatis.generator.internal.DefaultShellCallback;


public class Generator {
		
	public static void generator() throws SQLException, IOException, InterruptedException, XMLParserException, InvalidConfigurationException, URISyntaxException{

		List<String> warnings = new ArrayList<String>();
		boolean overwrite = true;
		File configFile = new File(Generator.class.getResource("/com/lw/ssm/common/mybatis/generator.xml").toURI());
		ConfigurationParser cp = new ConfigurationParser(warnings);
		Configuration config = cp.parseConfiguration(configFile);
		DefaultShellCallback callback = new DefaultShellCallback(overwrite);
		MyBatisGenerator myBatisGenerator = new MyBatisGenerator(config, callback, warnings);
		myBatisGenerator.generate(null);
	}
		
	public static void main(String[] args) throws SQLException, IOException, InterruptedException, XMLParserException, InvalidConfigurationException, URISyntaxException {
		generator();
		
		System.out.println("========生成完成========");
	}

}
