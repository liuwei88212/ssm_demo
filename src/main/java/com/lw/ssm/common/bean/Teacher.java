package com.lw.ssm.common.bean;

import java.io.Serializable;

/**
 * 
 * 
 * 表名：t_teacher1
 * @author Administrator 2016-11-01 13:51:30
 */
public class Teacher implements Serializable{

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
     * 
     * 表字段 : id
     */
    private Long id;

    /**
     * 
     * 表字段 : uuid
     */
    private String uuid;

    /**
     * 
     * 表字段 : t_name
     */
    private String tName;

    /**
     * 
     * 表字段 : t_password
     */
    private String tPassword;

    /**
     * 
     * 表字段 : t_sex
     */
    private String tSex;

    /**
     * 
     * 表字段 : description
     */
    private String description;

    /**
     * 
     * 表字段 : pic_url
     */
    private String picUrl;

    /**
     * 
     * 表字段 : school_name
     */
    private String schoolName;

    /**
     * 
     * 表字段 : regist_date
     */
    private String registDate;

    /**
     * 
     * 表字段 : remark
     */
    private String remark;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid == null ? null : uuid.trim();
    }

    public String gettName() {
        return tName;
    }

    public void settName(String tName) {
        this.tName = tName == null ? null : tName.trim();
    }

    public String gettPassword() {
        return tPassword;
    }

    public void settPassword(String tPassword) {
        this.tPassword = tPassword == null ? null : tPassword.trim();
    }

    public String gettSex() {
        return tSex;
    }

    public void settSex(String tSex) {
        this.tSex = tSex == null ? null : tSex.trim();
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description == null ? null : description.trim();
    }

    public String getPicUrl() {
        return picUrl;
    }

    public void setPicUrl(String picUrl) {
        this.picUrl = picUrl == null ? null : picUrl.trim();
    }

    public String getSchoolName() {
        return schoolName;
    }

    public void setSchoolName(String schoolName) {
        this.schoolName = schoolName == null ? null : schoolName.trim();
    }

    public String getRegistDate() {
        return registDate;
    }

    public void setRegistDate(String registDate) {
        this.registDate = registDate == null ? null : registDate.trim();
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark == null ? null : remark.trim();
    }
}