package com.lw.ssm.dubbo.impl;

import com.lw.ssm.dubbo.DemoService;

public class DemoServiceImpl implements DemoService {

	public String hello(String name) {
		
		return "你好, "+name;
	}

}
