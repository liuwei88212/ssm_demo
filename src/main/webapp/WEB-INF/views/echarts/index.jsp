<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>图标展示</title>
	
	<script type="text/javascript" src="${pageContext.request.contextPath}/js/echarts/dist/echarts.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-1.8.0.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/js/echarts/theme/macarons.js"></script>
</head>
<body>
	<div align="center">
		<jsp:include page="/WEB-INF/views/echarts/pie.jsp" />
		<jsp:include page="/WEB-INF/views/echarts/line.jsp" />
		<jsp:include page="/WEB-INF/views/echarts/scatter-2.jsp" />
		<jsp:include page="/WEB-INF/views/echarts/scatter-line.jsp" />
	</div>
</body>
</html>