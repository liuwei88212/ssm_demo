<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<div id="scatterDiv" style="width: 600px;height:400px"></div>

<script type="text/javascript">
	function getNumberInNormalDistribution(mean, std_dev) {
		return mean + (randomNormalDistribution() * std_dev);
	}
	
	function randomNormalDistribution() {
		var u = 0.0, v = 0.0, w = 0.0, c = 0.0;
		do {
			//获得两个（-1,1）的独立随机变量
			u = Math.random() * 2 - 1.0;
			v = Math.random() * 2 - 1.0;
			w = u * u + v * v;
		} while (w == 0.0 || w >= 1.0)
		
		
		//这里就是 Box-Muller转换
		c = Math.sqrt((-2 * Math.log(w)) / w);
		//返回2个标准正态分布的随机数，封装进一个数组返回
		//当然，因为这个函数运行较快，也可以扔掉一个
			//return [u*c, v*c];
		return u * c;
	}
	
	/**test 2*/
	function getNumberInNormalDistribution2(mean,std_dev){    
	    return mean+(uniform2NormalDistribution()*std_dev);
	}

	function uniform2NormalDistribution(){
	    var sum=0.0;
	    for(var i=0; i<12; i++){
	        sum=sum+Math.random();
	    }
	    return sum-6.0;
	}
	
	var scatterChart = echarts.init(document.getElementById('scatterDiv'));
	scatterChart.setOption({
		title: {
			text: '身高分布散列图',
		},
		tooltip: {
			trigger: 'axis',
			showDelay: 0,
			formatter: function(params) {
				if(params.value.length > 1) {
					return params.seriesName + ' :<br/>' +
						params.value[0] + 'cm  ：' +
						params.value[1] ;
				} else {
					return params.seriesName + ' :<br/>' +
						params.name + 'cm : ' +
						params.value;
				}
			},
			axisPointer: {
				show: true,
				type: 'cross',
				lineStyle: {
					type: 'dashed',
					width: 1
				}
			}
		},
		legend: {
			data: ['男性']
		},
		toolbox: {
			show: true,
			feature: {
				mark: {
					show: true
				},
				dataZoom: {
					show: true
				},
				dataView: {
					show: true,
					readOnly: false
				},
				restore: {
					show: true
				},
				saveAsImage: {
					show: true
				}
			}
		},
		xAxis: [{
			type: 'value',
			scale: true,
			axisLabel: {
				formatter: '{value} cm'
			}
		}],
		yAxis: [{
			type: 'value',
			scale: true,
			axisLabel: {
				formatter: '{value}'
			}
		}],
		series: [{
			name: '男性',
			type: 'scatter',
			symbolSize: 5,
			data: []
		}]
	});
	
	var miu = 170;
	var simga = 10;
	var map = {};
	for(var i=0; i<10000; i++){
	    temp = Math.floor(getNumberInNormalDistribution2(miu, simga));
	    
	    if(map[temp]){
	    	map[temp] = map[temp]+1;	
	    }else{
	    	map[temp] = 1;
	    }
	}
	
	scatterChart.showLoading();
	$(function() {
		var   ary   =   new   Array(); 
		
		for(var prop in map){
		    if(map.hasOwnProperty(prop)){
		        //console.log( prop +'->' + map[prop]);
		        ary.push(new Array(parseInt(prop), map[prop]))
		    }
		}
		console.log( ary);
		
		scatterChart.setOption({
             series: [{
                 data: ary
             }]
         });
		 
		scatterChart.hideLoading();    //隐藏加载动画
	});
</script>