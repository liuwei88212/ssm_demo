<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>


<div id="pieDiv" style="width: 600px;height:400px">
	
</div>
<script type="text/javascript">
	var pieDiv = echarts.init(document.getElementById('pieDiv'));
	pieDiv.setOption({
		title: {
			text: '饼图',
			x: 'center'
		},
		tooltip: {},
		legend: {
			orient: 'vertical',
			left: 'left',
			data: ['衬衫', '羊毛衫', '雪纺衫', '裤子', '袜子', '高跟鞋']
		},
		series: [{
			name: '销量',
			radius: '65%',
			center: ['50%', '50%'],
			type: 'pie',
			data: [{
				value: 335,
				name: '衬衫'
			}, {
				value: 310,
				name: '羊毛衫'
			}, {
				value: 234,
				name: '雪纺衫'
			}, {
				value: 135,
				name: '裤子'
			}, {
				value: 1548,
				name: '袜子'
			}, {
				value: 1548,
				name: '高跟鞋'
			}],
			itemStyle: {
				normal: {
					label: {
						show: true,
						formatter: '{b} : {c} \n ({d}%)'
					},
					labelLine: {
						show: true
					}
				},
			}
		}]
	});

</script>    