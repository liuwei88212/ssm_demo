<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<div id="scatterLineDiv" style="width: 600px;height:400px"></div>

<script type="text/javascript">
	

	var div = echarts.init(document.getElementById('scatterLineDiv'));
	var markLineOpt = {
	        animation: true,
	        label: {
	            normal: {
	                formatter: 'y = 0.5 * x + 3',
	                textStyle: {
	                    align: 'right'
	                }
	            }
	        },
	        lineStyle: {
	            normal: {
	                type: 'solid'
	            }
	        },
	        tooltip: {
	            formatter: 'y = 0.5 * x + 3'
	        },
	        data: [[{
	            coord: [0, 3],
	            symbol: 'none'
	        }, {
	            coord: [20, 13],
	            symbol: 'none'
	        }]]
	    };
	
	div.setOption({
		title: {
	        text: 'Anscombe\'s quartet',
	        x: 'center',
	        y: 0
	    },
	    tooltip: {
	        formatter: '{a}: ({c})'
	    },
	    toolbox: {
			show: true,
			feature: {
				mark: {
					show: true
				},
				dataView: {
					show: true,
					readOnly: false
				},
				restore: {
					show: true
				},
				saveAsImage: {
					show: true
				}
			}
		},
	    xAxis: [
			{gridIndex: 0, min: 0, max: 20}
	    ],
	    yAxis: [
			{gridIndex: 0, min: 0, max: 20}
	    ],
	    series: [
	        {
	            name: '坐标',
	            type: 'scatter',
	            xAxisIndex: 0,
	            yAxisIndex: 0,
	            data: [
	                   [10.0, 8.04],
	                   [8.0, 6.95],
	                   [13.0, 7.58],
	                   [9.0, 8.81],
	                   [11.0, 8.33],
	                   [14.0, 9.96],
	                   [6.0, 7.24],
	                   [4.0, 4.26],
	                   [12.0, 10.84],
	                   [7.0, 4.82],
	                   [5.0, 5.68]
	               ],
	            markLine: markLineOpt
	        }
	    ]
	});
	
	
</script>