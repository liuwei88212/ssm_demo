<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<div id="scatterDiv" style="width: 600px;height:400px"></div>

<script type="text/javascript">
	
	var scatterChart = echarts.init(document.getElementById('scatterDiv'));
	scatterChart.setOption({
		title: {
			text: '身高分布散列图',
		},
		tooltip: {
			trigger: 'axis',
			formatter: function(params) {
				if(params.value.length > 1) {
					return params.seriesName + ' :<br/>' +
						params.value[0] + 'cm  ：' +
						params.value[1] ;
				} else {
					return params.seriesName + ' :<br/>' +
						params.name + 'cm : ' +
						params.value;
				}
			},
			axisPointer: {
				show: true,
				type: 'cross',
				lineStyle: {
					type: 'dashed',
					width: 1
				}
			}
		},
		legend: {
			data: ['男性','降水量']
		},
		toolbox: {
			show: true,
			feature: {
				mark: {show: true},
				dataZoom: {show: true},
				dataView: {show: true,readOnly: false},
				restore: {show: true},
				saveAsImage: {show: true}
			}
		},
		xAxis: [{
			type: 'value',
			scale: true,
			axisLabel: {
				formatter: '{value} cm'
			}
		}],
		yAxis: [{
			type: 'value',
			scale: true,
			axisLabel: {
				formatter: '{value}'
			}
		}],
		series: [{
			name: '男性',
			type: 'scatter',
			symbolSize: 6,
			data: []
		},{
            name:'降水量',
            type:'line',
            data:[0.0, 2.2, 3.3, 4.5, 6.3, 10.2, 20.3, 23.4, 23.0, 16.5, 12.0, 6.2]
        }]
	});
	
	$(function() {
		scatterChart.showLoading();
		$.ajax({  
            type : "post",  
            url : "${pageContext.request.contextPath}/echarts/scatter",  
            dataType : "json", //返回数据形式为json  
            success : function(result) {  
                if (result) {  
                	scatterChart.setOption({
                        series: [{
                            // 根据名字对应到相应的系列
                            data: result.series[0].dataAryObj
                        }]
                    });
                    scatterChart.hideLoading();    //隐藏加载动画
                }  
            },  
            error : function(errorMsg) {  
                alert("不好意思，图表请求数据失败啦!");  
            }  
        });
	});
</script>