<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<div id="lineDiv" style="width: 600px;height:400px">
	
</div>    
    
<script type="text/javascript">
	var lineDiv = echarts.init(document.getElementById('lineDiv'), 'macarons');
	
	lineDiv.setOption({
		title: {
			text: '未来一周气温变化',
		},
		tooltip: {
			trigger: 'axis'
		},
		legend: {
			 data:['最高温度']
		},
		toolbox: {
			show: true,
			feature: {
				mark: {
					show: true
				},
				dataView: {
					show: true,
					readOnly: false
				},
				magicType: {
					show: true,
					type: ['line', 'bar', ]
				},
				saveAsImage: {
					show: true
				}
			}
		},
		calculable: true,
		xAxis: [{
			type: 'category',
			data: []
		}],
		yAxis: [{
			type: 'value',
			axisLabel : {
                formatter: '{value} °C'
            }
		}],
		series: [{
			name: '最高温度',
			type: 'line',
			data: [],
			
			//symbol:'none',  //这句就是去掉点的  
			smooth:true,  //这句就是让曲线变平滑的  
			label: {
				normal: {
					show: true,
					position: 'top'
				}
			},
			markPoint : {
                data : [
                    {type : 'max', name: '最大值'},
                    {type : 'min', name: '最小值'}
                ]
	        },
	        markLine : {
                data : [
                    {type : 'average', name: '平均值'}
                ]
            },
			itemStyle: {
				normal: {
					color: function(params) {
						var colorList = [
							'#C1232B', '#B5C334', '#FCCE10', '#E87C25', '#27727B',
							'#FE8463', '#9BCA63', '#FAD860', '#F3A43B', '#60C0DD',
							'#D7504B', '#C6E579', '#F4E001', '#F0805A', '#26C0C0'
						];
						return colorList[params.dataIndex]
					}
				}
			}
		}]
	});
	
	lineDiv.showLoading();
	
	$(function(){
		//获得图表的options对象  
        //var options = lineDiv.getOption();  
        //通过Ajax获取数据  
        $.ajax({  
            type : "post",  
            url : "${pageContext.request.contextPath}/echarts/line",  
            dataType : "json", //返回数据形式为json  
            success : function(result) {  
                if (result) {  
                    // options.legend.data = result.legend;  
                    //options.xAxis[0].data = result.category;  
                    //options.series[0].name = result.series[0].name;  
                    //options.series[0].type = result.series[0].type;
                    //options.series[0].data = result.series[0].data;  
                    
                    //lineDiv.setOption(options);  
                    
                    lineDiv.setOption({
                    	legend:{
                    		data: result.legend
                    	},
                        xAxis: {
                            data: result.category
                        },
                        series: [{
                        	name: result.series[0].name,
                			type: result.series[0].type,
                            // 根据名字对应到相应的系列
                            data: result.series[0].data
                        }]
                    });
                    lineDiv.hideLoading();    //隐藏加载动画
                }  
            },  
            error : function(errorMsg) {  
                alert("不好意思，大爷，图表请求数据失败啦!");  
            }  
        });  
	})
		
</script>