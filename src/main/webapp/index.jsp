<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<head>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-1.8.0.js"></script>

<script type="text/javascript">

	function getNumberInNormalDistribution(mean, std_dev) {
		return mean + (randomNormalDistribution() * std_dev);
	}

	function randomNormalDistribution() {
		var u = 0.0, v = 0.0, w = 0.0, c = 0.0;
		do {
			//获得两个（-1,1）的独立随机变量
			u = Math.random() * 2 - 1.0;
			v = Math.random() * 2 - 1.0;
			w = u * u + v * v;
		} while (w == 0.0 || w >= 1.0)
		
		
		//这里就是 Box-Muller转换
		c = Math.sqrt((-2 * Math.log(w)) / w);
		//返回2个标准正态分布的随机数，封装进一个数组返回
		//当然，因为这个函数运行较快，也可以扔掉一个
 		//return [u*c, v*c];
		return u * c;

	}

	var inputMatrix = [];
	function polyfit(userInput) {
		var returnResult = [];
		inputMatrix = [];
		var n = userInput.length;
		for (var i = 0; i < n; i++) {
			var tempArr = [];
			for (var j = 0; j < n; j++) {
				tempArr.push(Math.pow(userInput[i].x, n - j - 1));
			}
			tempArr.push(userInput[i].y);
			inputMatrix.push(tempArr);
		}
		for (var i = 0; i < n; i++) {
			var base = inputMatrix[i][i];
			for (var j = 0; j < n + 1; j++) {
				if (base == 0) {
					//存在相同x不同y的点，无法使用多项式进行拟合
					return false;
				}
				inputMatrix[i][j] = inputMatrix[i][j] / base;
			}
			for (var j = 0; j < n; j++) {
				if (i != j) {
					var baseInner = inputMatrix[j][i];
					for (var k = 0; k < n + 1; k++) {
						inputMatrix[j][k] = inputMatrix[j][k] - baseInner
								* inputMatrix[i][k];
					}
				}
			}
		}
		for (var i = 0; i < n; i++) {
			if (inputMatrix[i][n] > 0) {
				returnResult.push('+');
			}

			if (inputMatrix[i][n] != 0) {
				var tmp_x = '';
				for (var j = 0; j < n - 1 - i; j++) {
					tmp_x = tmp_x + "*x";
				}
				returnResult.push((inputMatrix[i][n] + tmp_x));
			}
		}
		return returnResult;
	}
	
	//这里使用 floor 函数向下取整，而不能使用 round 函数四舍五入取整，round 取整后得到的是非均匀分布。
	//http://www.cnblogs.com/zztt/p/4024906.html
// 	var temp;
// 	var arr=[0,0,0,0,0,0,0,0,0,0];
// 	for(var i=0;i<1000000000;i++){
// 	    temp=Math.floor(Math.random()*10+1);
// 	    arr[temp]=arr[temp]+1;
// 	}
	
	var miu = 170;
	var simga = 10;
	var map = {};
	for(var i=0; i<1000000; i++){
	    temp = Math.floor(getNumberInNormalDistribution(miu, simga));
	    
	    if(map[temp]){
	    	map[temp] = map[temp]+1;	
	    }else{
	    	map[temp] = 1;
	    }
	}
	
	var   a   =   new   Array(); 
	$(function() {
		for(var prop in map){
		    if(map.hasOwnProperty(prop)){
		        console.log( prop +'->' + map[prop]);
		        
		        a.push(new Array(prop, map[prop]))
		    }
		}
		console.log( a);
// 		for (var j = 0; j < arr.length; j++) {
// 		    console.log("得到"+j+"的概率:" + arr[j]/1000000000);
// 		}
	});
</script>

</head>

<body>
<h1>Hello World</h1>
</body>