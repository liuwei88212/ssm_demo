
package com.lw.ssm.test.hessian;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

public class HessianTest
{
    public static void main(String[] args) throws IOException, ClassNotFoundException
    {
        ByteArrayOutputStream os = new ByteArrayOutputStream();

        ObjectOutputStream out = new ObjectOutputStream(os);

        out.writeObject(new Person(30, "刘维"));

        byte[] personAry = os.toByteArray();

        ByteArrayInputStream is = new ByteArrayInputStream(personAry);
        ObjectInputStream in = new ObjectInputStream(is);

        Person p = (Person) in.readObject();

        System.out.println(p.getAge() + "," + p.getName());
    }
}

class Person implements Serializable
{

    private static final long serialVersionUID = 1L;

    String name;

    int age;

    public Person(int age, String name)
    {
        this.age = age;
        this.name = name;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public int getAge()
    {
        return age;
    }

    public void setAge(int age)
    {
        this.age = age;
    }

}