package com.lw.ssm.test.proxy;

public class TestProxy
{
    public static void main(String[] args)
    {
        Person person = new PersonImpl();
        DynamicProxy proxy = new DynamicProxy(person);
        
        Person personProxy = proxy.getTarget();
        personProxy.say();
        
        System.out.println(personProxy.getClass().getName());
    }
}
