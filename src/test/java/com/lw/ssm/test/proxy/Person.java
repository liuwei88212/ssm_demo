package com.lw.ssm.test.proxy;

public interface Person
{
    String say();
}
