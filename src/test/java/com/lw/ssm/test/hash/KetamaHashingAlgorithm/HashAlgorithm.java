
package com.lw.ssm.test.hash.KetamaHashingAlgorithm;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public enum HashAlgorithm {

    /**
     * MD5-based hash algorithm used by ketama.
     */
    KETAMA_HASH;

    public long hash(byte[] digest , int nTime)
    {

        //对于每四个字节，组成一个long值数值，做为这个虚拟节点的在环中的惟一key  
        long rv = ((long) (digest[3 + nTime * 4] & 0xFF) << 24) | ((long) (digest[2 + nTime * 4] & 0xFF) << 16)
                | ((long) (digest[1 + nTime * 4] & 0xFF) << 8) | (digest[0 + nTime * 4] & 0xFF);

//        System.out.println("rv=" + rv);

        return rv & 0xffffffffL; /* Truncate to 32-bits */
    }

    /**
     * Get the md5 of the given key.
     */
    public byte[] computeMd5(String k)
    {
        MessageDigest md5;
        try
        {
            md5 = MessageDigest.getInstance("MD5");
        }
        catch (NoSuchAlgorithmException e)
        {
            throw new RuntimeException("MD5 not supported", e);
        }
        md5.reset();
        byte[] keyBytes = null;
        try
        {
            keyBytes = k.getBytes("UTF-8");
        }
        catch (UnsupportedEncodingException e)
        {
            throw new RuntimeException("Unknown string :" + k, e);
        }

        md5.update(keyBytes);
        return md5.digest();
    }

    public static void main(String[] args) throws UnsupportedEncodingException
    {
        String s = "1";

        byte[] md5 = HashAlgorithm.KETAMA_HASH.computeMd5(s);

        System.out.println("md5 = " + md5.length);

        System.out.println(HashAlgorithm.KETAMA_HASH.hash(md5, 0));

        System.out.println(HashAlgorithm.KETAMA_HASH.hash(md5, 3));
    }
}
