/**  
 * 
 * @Title:  Node.java   
 * @Package com.lw.vrv.javacore.hashcode   
 * @Description:    TODO
 * @author: BruceLiu     
 * @date:   2017年5月23日 下午6:12:47   
 * @version V1.0 
 */
package com.lw.ssm.test.hash.Consistent;

/**
 * @ClassName: Node
 * @Package com.lw.vrv.javacore.hashcode
 * @Description:TODO(用一句话描述该文件做什么)
 * @author: BruceLiu
 * @date: 2017年5月23日 下午6:12:47
 */
public class Node {

    public String IP;
    
    public int port;
    

    public Node(String iP) {
        this.IP = iP;
    }
    
    public Node(String iP, int port) {
        this.IP = iP;
        this.port = port;
    }
    
    
    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    /**
     * @return the iP
     */
    public String getIP() {
        return IP;
    }

    /**
     * @param iP the iP to set
     */
    public void setIP(String iP) {
        IP = iP;
    }

    @Override
    public String toString() {
        return IP;
    }

}
