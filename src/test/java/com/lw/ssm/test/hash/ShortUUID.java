package com.lw.ssm.test.hash;

import java.util.UUID;

public class ShortUUID {
	public static String[] chars = new String[] { "10", "20", "30", "40", "50", "60", "70", "80", "90", "11", "21",
			"31", "41", "51", "61", "71", "81", "91", "12", "22", "32", "42", "52", "62", "72", "82", "92", "13", "23",
			"33", "43", "53", "63", "73", "83", "93", "14", "24", "34", "44", "54", "64", "74", "84", "94", "15", "25",
			"35", "45", "55", "65", "75", "85", "95", "16", "26", "36", "46", "56", "66", "76", "86", "96", "17", "27",
			"37", "47", "57", "67", "77", "87", "97", "18", "28", "38", "48", "58", "68", "78", "88", "98", "19", "29",
			"39", "49", "59", "69", "79", "89", "99", };

	public static String generateShortUuid() {
		StringBuffer shortBuffer = new StringBuffer();
		String uuid = UUID.randomUUID().toString().replace("-", "");
		for (int i = 0; i < 8; i++) {
			String str = uuid.substring(i * 4, i * 4 + 4);
			int x = Integer.parseInt(str, 16);
			//shortBuffer.append(chars[x % 0x3E]).append("-");
			shortBuffer.append(chars[x % 0x3E]);
		}
		//shortBuffer = shortBuffer.deleteCharAt(shortBuffer.lastIndexOf("-"));
		return shortBuffer.toString();

	}

	public static void main(String[] args) {
		 //for (int i = 0; i < 10; i++) {
			String s = generateShortUuid();
			System.out.println(s);
//		}
	}
}
