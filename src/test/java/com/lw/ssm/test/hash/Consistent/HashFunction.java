/**  
 * 
 * @Title:  HashFunction.java   
 * @Package com.lw.vrv.javacore.hashcode   
 * @Description:    TODO
 * @author: BruceLiu     
 * @date:   2017年5月23日 下午5:07:37   
 * @version V1.0 
 */
package com.lw.ssm.test.hash.Consistent;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import com.twmacinta.util.MD5;

/**
 * @ClassName: HashFunction
 * @Package com.lw.vrv.javacore.hashcode
 * @Description:TODO(用一句话描述该文件做什么)
 * @author: BruceLiu
 * @date: 2017年5月23日 下午5:07:37
 */
public class HashFunction {
    private MessageDigest md5 = null;
    
    public static void main(String[] args) {
        HashFunction hashFunction  = new HashFunction();
        
        System.out.println(hashFunction.hash("111"));
        
        System.out.println(hashFunction.hash2("111"));
    }

    public int hash(Object obj) {
        MD5 md5 = new MD5();
        try {
            md5.Update(obj.toString(), null);
        } catch (UnsupportedEncodingException e) {
            throw new IllegalStateException("NoSuchAlgorithmException:MD5", e);
        }
        return Math.abs(md5.asHex().hashCode());
    }

    public long hash2(String key) {
        if (md5 == null) {
            try {
                md5 = MessageDigest.getInstance("MD5");
            } catch (NoSuchAlgorithmException e) {
                throw new IllegalStateException("no md5 algorythm found");
            }
        }

        md5.reset();
        md5.update(key.getBytes());
        byte[] bKey = md5.digest();
        long res = ((long) (bKey[3] & 0xFF) << 24) | ((long) (bKey[2] & 0xFF) << 16) | ((long) (bKey[1] & 0xFF) << 8)
                | (long) (bKey[0] & 0xFF);
        return res & 0xffffffffL;
    }

}
