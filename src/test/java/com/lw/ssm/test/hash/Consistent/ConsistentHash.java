/**  
 * 
 * @Title:  ConsistentHash.java   
 * @Package com.lw.vrv.javacore.hashcode   
 * @Description:    TODO
 * @author: BruceLiu     
 * @date:   2017年5月23日 下午5:03:44   
 * @version V1.0 
 */
package com.lw.ssm.test.hash.Consistent;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

/**
 * 一致性哈希 JAVA算法实现
 * 
 * @ClassName: ConsistentHash
 * @Package com.lw.vrv.javacore.hashcode
 * @Description:TODO(用一句话描述该文件做什么)
 * @author: BruceLiu
 * @date: 2017年5月23日 下午5:03:44
 */
public class ConsistentHash<T> {
    private final HashFunction hashFunction;

    // 虚拟节点
    private final int numberOfReplicas;

    // 用来存储虚拟节点hash值 到真实node的映射
    private final SortedMap<Integer, T> circle = new TreeMap<Integer, T>();

    public ConsistentHash(HashFunction hashFunction, int numberOfReplicas,
            Collection<T> nodes) {
        this.hashFunction = hashFunction;
        this.numberOfReplicas = numberOfReplicas;

        for (T node : nodes) {
            add(node);
        }
    }

    public void add(T node) {
        System.out.println("初始化Hash");
        for (int i = 0; i < numberOfReplicas; i++) {
            String key = node.toString() + "#" + i;
            System.out.println("key=" + key);
            System.out.println("hash(key)=" + hashFunction.hash(key));
            circle.put(hashFunction.hash(key), node);
        }
        System.out.println("=====");
    }

    public void remove(T node) {
        for (int i = 0; i < numberOfReplicas; i++) {
            circle.remove(hashFunction.hash(node.toString() + i));
        }
    }

    /**
     * 获得一个最近的顺时针节点
     * 
     * @param key 为给定键取Hash，取得顺时针方向上最近的一个虚拟节点对应的实际节点
     * @return
     */
    public T get(Object key) {
        if (circle.isEmpty()) {
            return null;
        }
        int hash = hashFunction.hash(key);
        if (!circle.containsKey(hash)) {
            //返回此映射的部分视图，其键大于等于 hash
            SortedMap<Integer, T> tailMap = circle.tailMap(hash);
            hash = tailMap.isEmpty() ? circle.firstKey() : tailMap.firstKey();
        }
        return circle.get(hash);
    }
    
    public int getSize(){
        return circle.size();
    }

    public static void main(String... arg) {
        List<Node> nodelist = new ArrayList<Node>();
        nodelist.add(new Node("192.168.0.1"));
        nodelist.add(new Node("192.168.0.2"));
        nodelist.add(new Node("192.168.0.3"));
        nodelist.add(new Node("192.168.0.4"));

        ConsistentHash<Node> ch = new ConsistentHash<Node>(new HashFunction(), 3, nodelist);
        Node node = ch.get("192.168.0.2#2");
        System.out.println("当前节点：" + node.toString()+","+ch.getSize());

        Iterator i = ch.circle.entrySet().iterator();
        while (i.hasNext()) {
            Map.Entry me = (Map.Entry) i.next();
            System.out.print(me.getKey() + ": ");
            System.out.println(me.getValue());
        }

        System.out.println(ch.circle.headMap(24691842));
        System.out.println(ch.circle.tailMap(24691840));
        System.out.println(ch.circle.tailMap(1414726947));
    }
}
