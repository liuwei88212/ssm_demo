
package com.lw.ssm.test.hash;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.SortedMap;
import java.util.TreeMap;

/**
 *  一致性哈希算法是分布式系统中常用的算法。
 *  比如，一个分布式的存储系统，要将数据存储到具体的节点上，如果采用普通的hash方法，将数据映射到具体的节点上，如key%N，key是数据的key，N是机器节点数，
 *  如果有一个机器加入或退出这个集群，则所有的数据映射都无效了，如果是持久化存储则要做数据迁移，如果是分布式缓存，则其他缓存就失效了。 
 *  一致性Hash算法将 value 映射到一个 32 为的 key 值，也即是 0~2^32-1 次方的数值空间；我们可以将这个空间想象成一个首（ 0 ）尾（ 2^32-1 ）相接的圆环。
 *   
 * @ClassName:  ConsistencyHash   
 * @Description:
 * @author: liuwei  
 * @date:   2016年7月26日 下午4:50:31
 */
public class ConsistencyHash
{
    private TreeMap<Long, Object> nodes = null;

    //真实服务器节点信息  
    private List<Object> shards = new ArrayList<Object>();

    //设置虚拟节点数目  
    private int VIRTUAL_NUM = 4;

    //节点数
    private int NODE_NUM = 5;

    /** 
     * 初始化一致环 
     */
    public void init()
    {
        //shards.add("192.168.0.0-服务器0");
        //shards.add("192.168.0.1-服务器1");
        //shards.add("192.168.0.2-服务器2");
        //shards.add("192.168.0.3-服务器3");
        //shards.add("192.168.0.4-服务器4");

        for (int i = 0; i < NODE_NUM; i++)
        {
            shards.add("Server" + i);
        }

        nodes = new TreeMap<Long, Object>();
        for (int i = 0; i < shards.size(); i++)
        {
            Object shardInfo = shards.get(i);

            for (int z = 0; z < 160 / VIRTUAL_NUM; z++)
            {

                byte[] digest = computeMd5(shardInfo + "" + z);

                for (int j = 0; j < VIRTUAL_NUM; j++)
                {
                    nodes.put(hash(digest, j), shardInfo);
                }
            }

        }
    }

    /** 
     * 根据key的hash值取得服务器节点信息 
     * @param hash 
     * @return 
     */
    public Object getShardInfo(long hash)
    {
        Long key = hash;

        //得到大于当前key的那个子Map，然后从中取出第一个key，就是大于且离它最近的那个key  
        SortedMap<Long, Object> tailMap = nodes.tailMap(key);
        if (tailMap.isEmpty())
        {
            key = nodes.firstKey();
        }
        else
        {
            key = tailMap.firstKey();
        }
        return nodes.get(key);
    }

    /** 
     * 打印圆环节点数据 
     */
    public void printMap()
    {
        System.out.println(nodes);
    }

    /** 
     * 根据2^32把节点分布到圆环上面。 
     * @param digest 
     * @param nTime 
     * @return 
     */
    public long hash(byte[] digest , int nTime)
    {
        long rv = ((long) (digest[3 + nTime * 4] & 0xFF) << 24) | ((long) (digest[2 + nTime * 4] & 0xFF) << 16)
                | ((long) (digest[1 + nTime * 4] & 0xFF) << 8) | (digest[0 + nTime * 4] & 0xFF);

        return rv & 0xffffffffL; /* Truncate to 32-bits */
    }

    /** 
     * Get the md5 of the given key. 
     * 计算MD5值 
     */
    public byte[] computeMd5(String k)
    {
        MessageDigest md5;
        try
        {
            md5 = MessageDigest.getInstance("MD5");
        }
        catch (NoSuchAlgorithmException e)
        {
            throw new RuntimeException("MD5 not supported", e);
        }
        md5.reset();
        byte[] keyBytes = null;
        try
        {
            keyBytes = k.getBytes("UTF-8");
        }
        catch (UnsupportedEncodingException e)
        {
            throw new RuntimeException("Unknown string :" + k, e);
        }

        md5.update(keyBytes);
        return md5.digest();
    }

    public static void main(String[] args)
    {
        Random ran = new Random();
        ConsistencyHash consistencyHash = new ConsistencyHash();
        //        System.out.println(consistencyHash.hash(consistencyHash.computeMd5("SHARD-1-NODE-1"), 2));

        consistencyHash.init();
        consistencyHash.printMap();

        //循环50次，是为了取50个数来测试效果，当然也可以用其他任何的数据来测试  
        Map<Object, Integer> map = new HashMap<Object, Integer>();

        int EXE_TIMES = 100;
        for (int i = 0; i < EXE_TIMES; i++)
        {
            Object value = consistencyHash.getShardInfo(consistencyHash.hash(
                    consistencyHash.computeMd5(String.valueOf(i)), ran.nextInt(consistencyHash.VIRTUAL_NUM)));

            System.out.println("随机数=" + i + ", " + value);

            Integer num = map.get(value);
            if (num == null)
            {
                map.put(value, 1);
            }
            else
            {
                map.put(value, num + 1);
            }
        }

        for (Map.Entry<Object, Integer> entry : map.entrySet())
        {
            System.out.println("节点名称 :" + entry.getKey() + " - 节点分配KEYS数量 : " + entry.getValue() + " - 百分比 : "
                    + (float) entry.getValue() / EXE_TIMES * 100 + "%");
        }
    }
}
