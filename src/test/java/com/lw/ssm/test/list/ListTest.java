package com.lw.ssm.test.list;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class ListTest
{
    public static void main(String[] args)
    {
        List list1 = new ArrayList();
        list1.add(1);
        list1.add(2);
        
        List list2 = new ArrayList();
        list2.add(3);
        list2.add(4);
        
        list2.addAll(list1);
        
        for (Iterator iterator = list2.iterator(); iterator.hasNext();)
        {
            Object object = (Object) iterator.next();
            
            System.out.println(object);
        }
        
    }
    
}
