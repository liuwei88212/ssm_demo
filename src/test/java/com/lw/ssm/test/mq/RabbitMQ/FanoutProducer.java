package com.lw.ssm.test.mq.RabbitMQ;

import java.util.concurrent.TimeoutException;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

public class FanoutProducer {
	public static void main(String[] argv) throws java.io.IOException,
			TimeoutException {

		Connection connection = null;
		Channel channel = null;
		try {
			/* 创建连接工厂 */
			ConnectionFactory factory = new ConnectionFactory();
			factory.setHost("localhost");
			/* 创建连接 */
			connection = factory.newConnection();
			/* 创建信道 */
			channel = connection.createChannel();

			String message = "hello world..."; // 需发送的信息

			/* 发送消息，使用默认的fanout交换器 */
			channel.basicPublish("amq.fanout", "", null, message.getBytes());
			System.out.println("Server: Send message -> " + message);

		} finally {
			/* 关闭连接、通道 */
			channel.close();
			connection.close();
			System.out.println("Closed the channel and conn.");
		}

	}
}
