package com.lw.ssm.test.mq.RabbitMQ;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.Consumer;
import com.rabbitmq.client.DefaultConsumer;
import com.rabbitmq.client.Envelope;

public class FanoutCustomerB {
	
	public static void main(String[] argv) throws java.io.IOException,
			java.lang.InterruptedException, TimeoutException {
		
		System.out.println("---------Customer B-----------");

		/* 创建连接工厂 */
		ConnectionFactory factory = new ConnectionFactory();
		factory.setHost("localhost");
		/* 创建连接 */
		Connection connection = factory.newConnection();
		/* 创建信道 */
		Channel channel = connection.createChannel();

		// 创建一个临时的、私有的、自动删除、随机名称的临时队列
		String queueName = channel.queueDeclare().getQueue();
		System.out.println("queue : " + queueName);
		channel.queueBind(queueName, "amq.fanout", "");
		System.out.println(FanoutCustomerB.class.getName()
				+ ", waiting for messages.");

		/* 定义消费者 */
		Consumer consumer = new DefaultConsumer(channel) {
			@Override
			public void handleDelivery(String consumerTag, Envelope envelope,
					AMQP.BasicProperties properties, byte[] body)
					throws IOException {
				String message = new String(body, "UTF-8");
				System.out.println("Received the message -> " + message);
			}
		};

		// 开始消费（设置自动确认消息）
		channel.basicConsume("", true, consumer);
	}
}
