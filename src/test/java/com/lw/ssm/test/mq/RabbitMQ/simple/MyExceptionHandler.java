package com.lw.ssm.test.mq.RabbitMQ.simple;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.Consumer;
import com.rabbitmq.client.ExceptionHandler;
import com.rabbitmq.client.TopologyRecoveryException;

public class MyExceptionHandler implements ExceptionHandler {

	public void handleUnexpectedConnectionDriverException(Connection conn,
			Throwable exception) {
		System.out
				.println("MyExceptionHandler.handleUnexpectedConnectionDriverException");
	}

	public void handleReturnListenerException(Channel channel,
			Throwable exception) {
		System.out.println("MyExceptionHandler.handleReturnListenerException");
	}

	public void handleFlowListenerException(Channel channel, Throwable exception) {
		System.out.println("MyExceptionHandler.handleFlowListenerException");
	}

	public void handleConfirmListenerException(Channel channel,
			Throwable exception) {
		System.out.println("MyExceptionHandler.handleConfirmListenerException");
	}

	public void handleBlockedListenerException(Connection connection,
			Throwable exception) {
		System.out.println("MyExceptionHandler.handleBlockedListenerException");
	}

	public void handleConsumerException(Channel channel, Throwable exception,
			Consumer consumer, String consumerTag, String methodName) {
		// 正常渠道应该有专业的LOG框架打印，此处简单处理
		exception.printStackTrace();
		System.out.println("MyExceptionHandler.handleConsumerException");
	}

	public void handleConnectionRecoveryException(Connection conn,
			Throwable exception) {
		System.out
				.println("MyExceptionHandler.handleConnectionRecoveryException");
	}

	public void handleChannelRecoveryException(Channel ch, Throwable exception) {
		System.out.println("MyExceptionHandler.handleChannelRecoveryException");
	}

	public void handleTopologyRecoveryException(Connection conn, Channel ch,
			TopologyRecoveryException exception) {
		System.out
				.println("MyExceptionHandler.handleTopologyRecoveryException");
	}

}
