package com.lw.ssm.test;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class ListTest
{
    public static void main(String[] args)
    {
        List<String> list = new ArrayList<String>();
        for(int i=0;i<10;i++){
            list.add(i+"");
        }
        System.out.println(list.size());
        
        Iterator<String> iter = list.iterator();
        while(iter.hasNext()){
            String s = iter.next();
            if(s.equals("1")){
                iter.remove();    
            }
        }
        
        System.out.println(list.size());
    }
}
