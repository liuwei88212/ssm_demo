package com.lw.ssm.test;

/**
 * java中cpu分给每个线程的时间片是随机的并且在java中好多都是多个线程共用一个资源，
 * 
 * 比如火车卖票，火车票是一定的，但卖火车票的窗口到处都有，每个窗口就相当于一个线程，这么多的线程共用所有的火车票这个资源。
 * 如果在一个时间点上，两个线程同时使用这个资源，那他们取出的火车票是一样的（座位号一样），这样就会给乘客造成麻烦。
 * 
 * @ClassName: ThreadTest
 * @Description:
 * @author: liuwei
 * @date: 2016年5月12日 下午4:53:10
 */

public class ThreadTest {
	public static void main(String args[]) {
		TicketSouce mt = new TicketSouce();
		// 基于火车票创建三个窗口
		new Thread(mt, "a").start();
		new Thread(mt, "b").start();
		new Thread(mt, "c").start();
	}
}

class TicketSouce implements Runnable {

	// 票的总数
	private int ticket = 10;

	public void run() {
		for (int i = 1; i < 50; i++) {
			synchronized (this) {
				if (ticket > 0) {
					// 休眠1s秒中，为了使效果更明显，否则可能出不了效果
					try {
						Thread.sleep(1000);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					System.out.println(Thread.currentThread().getName()
							+ "号窗口卖出" + this.ticket-- + "号票");
				}
			}
		}
	}
}
