package com.lw.ssm.test;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.util.Calendar;


public class SQLBatchTest
{
    public static PreparedStatement ps;
    
    public static Connection conn;
    
    public static void main(String[] args) throws Exception {  
        
        String driver = "com.mysql.jdbc.Driver";    
        String url = "jdbc:mysql://192.168.8.28:3306/liuwei?useUnicode=true&characterEncoding=UTF-8&createDatabaseIfNotExist=true";    
        String user = "root";    
        String password = "123";    
    
        Class.forName(driver);    
        conn = DriverManager.getConnection(url, user, password);
        
        
        String empSQL = "insert into emp (ename, job, mgr, hiredate, sal, comm, deptno) values (?, ?, ?, ?, ?, ?, ?)";
        
        String deptSQL = "insert into dept (deptno, dname, loc) values (?, ?, ?)";
        
        //EMP 200W数据
        for(int j=0; j<20; j++){
            
            long start = System.currentTimeMillis();
            conn.setAutoCommit(false);
            ps = conn.prepareStatement(empSQL);
            for(int i=0; i< 100000; i++){
                ps.setString(1, "name_"+i);
                ps.setString(2, "job_"+i);
                ps.setInt(3, 1);
                ps.setDate(4, new Date(Calendar.getInstance().getTime().getTime()));
                ps.setBigDecimal(5, new BigDecimal(Math.random()*10000));
                ps.setBigDecimal(6, new BigDecimal(Math.random()*1000));
                ps.setInt(7, (int)Math.floor(Math.random()*100000));
                
                ps.addBatch();
            }
            ps.executeBatch();
            conn.commit();
            
            long end = System.currentTimeMillis();
            
            System.out.println("===>EMP表数据："+100000*(j+1)+", 执行时间："+(end-start)/1000);
        }
        
        long start = System.currentTimeMillis();
        conn.setAutoCommit(false);
        ps = conn.prepareStatement(deptSQL);
        for(int i=0; i<100000; i++){
            ps.setInt(1, (i+1));
            ps.setString(2, "dname_"+i);
            ps.setString(3, "loc_"+i);
            
            ps.addBatch();
        }
        ps.executeBatch();
        conn.commit();
        
        long end = System.currentTimeMillis();
        
        System.out.println("===>DEPT表数据："+100000+", 执行时间："+(end-start)/1000);
        
        ps.close();
        conn.close();  
    }  
}
