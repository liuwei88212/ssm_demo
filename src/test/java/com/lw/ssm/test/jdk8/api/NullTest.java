package com.lw.ssm.test.jdk8.api;

import java.util.Optional;

public class NullTest {
	
	public static void main(String[] args) {
		Optional< String > fullName = Optional.ofNullable( "Tom" );
		
		System.out.println( "Full Name is set? " + fullName.isPresent() );        
		System.out.println( "Full Name: " + fullName.orElseGet( () -> "[none]" ) ); 
		System.out.println( fullName.map( s -> "Hey " + s + "!" ).orElse( "Hey Stranger!" ) );
	}
}
