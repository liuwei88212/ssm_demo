package com.lw.ssm.test.jdk8.interf;

/**
 * Defaulable接口使用关键字default定义了一个默认方法notRequired()。DefaultableImpl类实现了这个接口，
 * 同时默认继承了这个接口中的默认方法；OverridableImpl类也实现了这个接口，但覆写了该接口的默认方法，并提供了一个不同的实现。 Java
 * 8带来的另一个有趣的特性是在接口中可以定义静态方法，例子代码如下：
 * 
 * @author Administrator
 *
 */
public interface Defaulable {

	// Interfaces now allow default methods, the implementer may or
	// may not implement (override) them.
	default String notRequired() {

		return "Default implementation";
	}
}
