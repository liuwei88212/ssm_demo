package com.lw.ssm.test.jdk8.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.util.ArrayList;
import java.util.Collection;

/**
 * 注解几乎可以使用在任何元素上：局部变量、接口类型、超类和接口实现类，甚至可以用在函数的异常定义上。
 * 
 * 
 * http://blog.csdn.net/yczz/article/details/50896975
 * 
 * @author Administrator
 *
 */
public class Annotations {
	
	@Retention(RetentionPolicy.RUNTIME)
	@Target({ ElementType.TYPE_USE, ElementType.TYPE_PARAMETER })
	public @interface NonEmpty {
	}

	public static class Holder<@NonEmpty T> extends @NonEmpty Object {
		public void method() throws @NonEmpty Exception {
		}
	}

	@SuppressWarnings("unused")
	public static void main(String[] args) {
		
		final Holder<String> holder = new @NonEmpty Holder<String>();
		
		@NonEmpty
		Collection<@NonEmpty String> strings = new ArrayList<>();
	}
}
