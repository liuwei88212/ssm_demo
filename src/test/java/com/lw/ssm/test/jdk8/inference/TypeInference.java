package com.lw.ssm.test.jdk8.inference;

public class TypeInference {
	
	public static void main(String[] args) {
		
		final Value<String> value = new Value<>();
		value.getOrDefault("22", Value.defaultValue());
	}
	
}
