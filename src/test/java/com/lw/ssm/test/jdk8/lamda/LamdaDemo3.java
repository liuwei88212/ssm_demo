package com.lw.ssm.test.jdk8.lamda;

import java.util.Arrays;
import java.util.List;

/**
 * Java 8中使用lambda表达式的Map和Reduce示例
 * 
 * 本例介绍最广为人知的函数式编程概念map。它允许你将对象进行转换。例如在本例中，我们将 costBeforeTax 列表的每个元素转换成为税后的值。我们将
 * x -> x*x lambda表达式传到 map() 方法，后者将其应用到流中的每一个元素。然后用 forEach()
 * 将列表元素打印出来。使用流API的收集器类，可以得到所有含税的开销。有 toList() 这样的方法将 map
 * 或任何其他操作的结果合并起来。由于收集器在流上做终端操作，因此之后便不能重用流了。你甚至可以用流API的 reduce()
 * 方法将所有数字合成一个，下一个例子将会讲到。
 * 
 * @author Administrator
 *
 */
public class LamdaDemo3 {
	public static void main(String[] args) {
		// 不使用lambda表达式为每个订单加上12%的税
		List<Integer> costBeforeTax = Arrays.asList(100, 200, 300, 400, 500);
		
//		t1(costBeforeTax);
		
		t2(costBeforeTax);
	}

	public static void t1(List<Integer> costBeforeTax) {

		for (Integer cost : costBeforeTax) {
			double price = cost + 0.2 * cost;
			System.out.println(price);
		}

		System.out.println("==================");
		// 使用lambda表达式
		costBeforeTax.stream().map((cost) -> cost + 0.2 * cost)
				.forEach(System.out::println);

		// costBeforeTax.stream().map((cost) -> cost + 0.2 * cost).forEach(n ->
		// System.out.println(n));
	}

	public static void t2(List<Integer> costBeforeTax) {
		// 为每个订单加上12%的税
		// 老方法：
		double total = 0;
		for (Integer cost : costBeforeTax) {
			double price = cost + 0.2 * cost;
			total = total + price;
		}

		System.out.println("Total : " + total);

		// 新方法：
		double bill = costBeforeTax.stream().map((cost) -> cost + 0.2 * cost)
				.reduce((sum, cost) -> sum + cost).get();
		System.out.println("Total : " + bill);
	}
}
