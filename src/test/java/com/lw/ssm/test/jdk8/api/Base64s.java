package com.lw.ssm.test.jdk8.api;

import java.nio.charset.StandardCharsets;
import java.util.Base64;

public class Base64s {
	public static void main(String[] args) {
//		final String text = "Base64 finally in Java 8! 你好";
		
		final String text = "http://192.168.9.10/ssm?a=1111&b=你好";

		final String encoded = Base64.getEncoder().encodeToString(text.getBytes(StandardCharsets.UTF_8));
		System.out.println(encoded);

		final String decoded = new String(Base64.getDecoder().decode(encoded), StandardCharsets.UTF_8);
		System.out.println(decoded);
		
		
		String encoded2 = Base64.getUrlEncoder().encodeToString(text.getBytes(StandardCharsets.UTF_8));
		System.out.println(encoded2);
		
		
		String decoded2 = new String(Base64.getUrlDecoder().decode(encoded2), StandardCharsets.UTF_8);
		System.out.println(decoded2);
	}
}
