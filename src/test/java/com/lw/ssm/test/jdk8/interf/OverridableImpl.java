package com.lw.ssm.test.jdk8.interf;

public class OverridableImpl implements Defaulable {

	public String notRequired() {
		return "Overridden implementation";
	}

	public static void main(String[] args) {
		Defaulable defaulable = new DefaultableImpl();
		String s = defaulable.notRequired();
		System.out.println(s);
	}
}
