package com.lw.ssm.test.设计模式.Observer;

/**
 *  A concrete subject
 */
import java.util.LinkedList;
import java.util.Vector;

public class ConcreteSubject implements Subject {
    private LinkedList observerList;
    private Vector strVector;
    
    public ConcreteSubject() {
        observerList =  new LinkedList();
        strVector = new Vector();
    }
    public void attach(Observer o) {
        observerList.add(o);
    }
    public void detach(Observer o) {
        observerList.remove(o);
    }
    public void sendNotify() {
        for(int i = 0; i < observerList.size(); i++) {
            ((Observer)observerList.get(i)).update(this);   
        }
    }
    public void setState(String action, String str) {
        if(action.equals("ADD")) {
            strVector.add(str);
            
        } else if(action.equals("DEL")) {
            
            strVector.remove(str);
        }
    }
    public Vector getState() {
        return strVector;
    }
}