package com.lw.ssm.test.设计模式.Observer;

public class HouseTest
{
    public static void main(String[] args)
    {
        House h = new House(100);
        
        HousePriceObserver A = new HousePriceObserver("张三");
        HousePriceObserver B = new HousePriceObserver("李四");
        HousePriceObserver C = new HousePriceObserver("王二麻");
        
        h.addObserver(A);
        h.addObserver(B);
        h.addObserver(C);
        
        
        System.out.println(h);
        h.setPrice(60);
        System.out.println(h);
    }
}
