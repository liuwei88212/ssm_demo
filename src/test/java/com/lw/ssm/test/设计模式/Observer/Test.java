package com.lw.ssm.test.设计模式.Observer;

/**
 *  A test client
 */
public class Test  {
    public static void main(String[] args) {
        Subject mySub = new ConcreteSubject();
        
        ObserverA myObserverA = new ObserverA(mySub);
        ObserverB myObserverB = new ObserverB();
        mySub.attach(myObserverA);
        mySub.attach(myObserverB);

        mySub.setState("ADD", "西瓜1");
        mySub.setState("ADD", "西瓜2");
        mySub.sendNotify();

        System.out.println("============================================");
        myObserverA.change("DEL", "西瓜2");
        myObserverA.change("ADD", "西瓜3");
        myObserverA.change("ADD", "西瓜4");
        myObserverA.notifySub();  
    }
}