
package com.lw.ssm.test.设计模式.Observer;

import java.util.Observable;

public class House extends Observable
{
    private float price;

    public House(float price)
    {
        this.price = price;
    }

    public float getPrice()
    {
        return price;
    }

    public void setPrice(float price)
    {
        super.setChanged();
        super.notifyObservers(price);
        this.price = price;
    }

    public String toString()
    {
        return "房子价格为：" + price;
    }
}
