package com.lw.ssm.test.设计模式.Observer;

/**
 *  Observer interface：抽象观察者角色
 */
public interface Observer {
    public void update(Subject s);
}