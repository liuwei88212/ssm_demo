
package com.lw.ssm.test.设计模式.Observer;

import java.util.Observable;

public class HousePriceObserver implements java.util.Observer
{
    private String name;

    public HousePriceObserver(String name)
    {
        this.name = name;
    }

    public void update(Observable o , Object arg)
    {
        if (arg instanceof Float)
        {
            System.out.println(this.name + "观察到价格变更为:" + arg);
        }
    }

}
