package com.lw.ssm.test.thread;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Date;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;

import com.lw.ssm.util.DateUtils;

/**
 * 多线程并发测试
 * 
 * @author Administrator
 * 
 */
public class ThreadConcurrentTest {

	final static int threadNum = 3000;
	
	final static AtomicInteger ATOMIC_INTEGER = new AtomicInteger(1);

	public static void main(String[] args) throws MalformedURLException,
			InterruptedException {
		// final URL url = new URL("http://www.baidu.com");
		final URL url = new URL("http://localhost:8089/EmptyServlet");

		final CyclicBarrier cyclicBarrier = new CyclicBarrier(threadNum);

		ExecutorService pool = Executors.newCachedThreadPool();

		System.out.println("==========任务开始===========");

		for (int i = 0; i < threadNum; i++) {
			pool.execute(new Runnable() {
				public void run() {
					try {
						// 等待所有任务准备就绪
						cyclicBarrier.await();

						String result = getURLSource(url);
						System.out.println(ATOMIC_INTEGER.getAndIncrement()+"->"+DateUtils.formatDateToString(new Date(), "yyyy-MM-dd HH:mm:ss:SSS")+ "->"+ result);

					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			});
		}

		pool.shutdown();
		while (!pool.isTerminated()) {
			try {
				Thread.sleep(10);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}

		System.out.println("==========任务结束===========");
	}

	/**
	 * 把二进制流转化为byte字节数组
	 * 
	 * @param instream
	 * @return byte[]
	 * @throws Exception
	 */
	public static byte[] readInputStream(InputStream instream) throws Exception {
		ByteArrayOutputStream outStream = new ByteArrayOutputStream();
		byte[] buffer = new byte[1024];
		int len = 0;
		while ((len = instream.read(buffer)) != -1) {
			outStream.write(buffer, 0, len);
		}
		instream.close();
		return outStream.toByteArray();
	}

	/**
	 * 通过网站域名URL获取该网站的源码
	 * 
	 * @param url
	 * @return String
	 * @throws Exception
	 */
	public static String getURLSource(URL url) throws Exception {
		HttpURLConnection conn = (HttpURLConnection) url.openConnection();
		conn.addRequestProperty("User-Agent",
				"Mozilla/5.0 (X11; U; Linux i586; en-US; rv:1.7.3) Gecko/20040924"
						+ "Epiphany/1.4.4 (Ubuntu)");

		conn.setRequestMethod("GET");
		conn.setConnectTimeout(5 * 1000);
		InputStream inStream = conn.getInputStream(); // 通过输入流获取html二进制数据
		byte[] data = readInputStream(inStream); // 把二进制数据转化为byte字节数据
		String htmlSource = new String(data);
		return htmlSource;
	}

}
