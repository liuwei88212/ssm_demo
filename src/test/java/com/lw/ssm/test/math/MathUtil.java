package com.lw.ssm.test.math;

import org.apache.commons.math3.distribution.NormalDistribution;

public class MathUtil {
	public static void main(String[] args) {
		NormalDistribution normalDistributioin = new NormalDistribution(0, 1);

		double cumulativeProbability = normalDistributioin
				.cumulativeProbability(1.0);

		System.out.println(cumulativeProbability);
	}
}
