package com.lw.ssm.test.math.spark;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.mllib.linalg.Vector;
import org.apache.spark.mllib.linalg.Vectors;
import org.apache.spark.mllib.stat.MultivariateStatisticalSummary;
import org.apache.spark.mllib.stat.Statistics;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.lw.ssm.charts.bean.MapBean;
import com.lw.ssm.charts.util.EchartsUtil;

/**
 * Spark 1.1.0 Basic Statistics（上）
 * 
 * http://blog.selfup.cn/1124.html
 * 
 * 
 * @author Administrator
 *
 */
public class Mllib_Demo5 {
	
	public static void main(String[] args) throws JsonProcessingException {
		Map<Integer, Double> randomDistributionMap = EchartsUtil.sumRandomDistribution(170d, 10d);
		List<MapBean> mapToList = EchartsUtil.mapToList(randomDistributionMap);
		
		ObjectMapper objMapper = new ObjectMapper();
		String writeValueAsString = objMapper.writeValueAsString(mapToList);
		
		System.out.println(writeValueAsString);
		
		System.out.println("map.size()=" + randomDistributionMap.size());
		
		System.setProperty("hadoop.home.dir","D:/tools/hadoop-2.5.2");
		
		
		SparkConf sparkConf = new SparkConf().setAppName("Statistics").setMaster("local[2]");
		JavaSparkContext sc = new JavaSparkContext(sparkConf);
		//JavaRDD<String> data = sc.textFile("/java/spark/statistics.txt");
		
		System.out.println("list.size()=" + mapToList.size());
		
		JavaRDD<MapBean> data =  sc.parallelize(mapToList);
		
		JavaRDD<Vector> parsedData = data.map(s -> {
			double[] values = Arrays.asList(s.getX()+"").stream().mapToDouble(Double::parseDouble).toArray();
			return Vectors.dense(values);
		});
		
		MultivariateStatisticalSummary summary = Statistics.colStats(parsedData.rdd());
		System.out.println("均值:" + summary.mean());
		System.out.println("方差:" + summary.variance());
		System.out.println("非零统计量个数:" + summary.numNonzeros());
		System.out.println("总数:" + summary.count());
		System.out.println("最大值:" + summary.max());
		System.out.println("最小值:" + summary.min());
		
		
		double square = 0f;//Xi平方的和double 
		double sum = 0f;//Xi的和
		double size = 1;
		double sigma = Math.sqrt((square - sum*sum/size)/(size-1));
	}
}
