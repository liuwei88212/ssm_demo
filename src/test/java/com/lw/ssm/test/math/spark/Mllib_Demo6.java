package com.lw.ssm.test.math.spark;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaDoubleRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.mllib.random.RandomRDDs;

/**
 * Spark 1.1.0 Basic Statistics（上）
 * 
 * http://blog.selfup.cn/1124.html
 * 
 * 
 * @author Administrator
 *
 */
public class Mllib_Demo6 {
	public static void main(String[] args) {
		SparkConf sparkConf = new SparkConf().setAppName("Statistics").setMaster("local[2]");
		JavaSparkContext sc = new JavaSparkContext(sparkConf);
		
		//生成100个随机数，N（0,1）标准正态分布，均匀分布在2个partition中
		JavaDoubleRDD n = RandomRDDs.normalJavaRDD(sc, 100L, 2);
		
		//生成100个随机数，期望和方差为10的泊松分布
		JavaDoubleRDD p = RandomRDDs.poissonJavaRDD(sc, 10, 100L);
		
		//生成100个随机数，0～1上的连续型均匀分布
		JavaDoubleRDD u = RandomRDDs.uniformJavaRDD(sc, 100);

		//将标准正态分布N（0,1）变成N（1,4）
		JavaDoubleRDD v = n.mapToDouble(x -> 1.0 + 2.0 * x);
		for(Double d : v.collect()) {
			System.out.print(d+" ");
		}
		
		System.out.println();
		for(Double d : p.collect()) {
			System.out.print(d+" ");
		}
		
		System.out.println();
		//将0～1均匀分布变为2～5均匀分布
		JavaDoubleRDD g = u.mapToDouble(x -> 2.0 + 3.0 * x);
		for(Double d : g.collect()) {
			System.out.print(d+" ");
		}
	}
}
