package com.lw.ssm.test.math.spark;

import java.util.Arrays;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.mllib.linalg.Vector;
import org.apache.spark.mllib.linalg.Vectors;
import org.apache.spark.mllib.stat.MultivariateStatisticalSummary;
import org.apache.spark.mllib.stat.Statistics;

/**
 * Spark 1.1.0 Basic Statistics（上）
 * 
 * http://blog.selfup.cn/1124.html
 * 
 * 
 * @author Administrator
 *
 */
public class Mllib_Demo3 {
	public static void main(String[] args) {
		
		System.setProperty("hadoop.home.dir","D:/tools/hadoop-2.5.2");
		
		
		SparkConf sparkConf = new SparkConf().setAppName("Statistics")
				.setMaster("local[2]");
		JavaSparkContext sc = new JavaSparkContext(sparkConf);
		JavaRDD<String> data = sc.textFile("/java/spark/statistics.txt");
		JavaRDD<Vector> parsedData = data.map(s -> {
			double[] values = Arrays.asList(s.split(" ")).stream()
					.mapToDouble(Double::parseDouble).toArray();
			return Vectors.dense(values);
		});
		
		MultivariateStatisticalSummary summary = Statistics.colStats(parsedData
				.rdd());
		System.out.println("均值:" + summary.mean());
		System.out.println("方差:" + summary.variance());
		System.out.println("非零统计量个数:" + summary.numNonzeros());
		System.out.println("总数:" + summary.count());
		System.out.println("最大值:" + summary.max().toArray().length);
		System.out.println("最小值:" + summary.min());
	}
}
