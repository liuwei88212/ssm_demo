package com.lw.ssm.test.math.normal;


/**
 * 正态函数求值算法的示例类,运行该类后，程序将打印出标准正态分布表,误差小于百万分之一
 * 
 * @author 欧阳康,北京师范大学教育技术学院,QQ:78692844
 * 
 */
public class NormalDistribution4 {
	/**
	 * 正态分布函数近似值，使用6项近似余函数
	 */
	private static double Fi_erf_6(double x) {
		double a = Math.abs(x);
		return 0.5 * (1 + erf_6(a / Math.sqrt(2)));
	}

	/**
	 * 正态分布函数六项级数近似余函数
	 */
	private static double erf_6(double x) {
		double a[] = { 0.070523084, 0.0422820123, 0.0092705272, 0.0001520143, 0.0002765672, 0.0000430638 };
		double t = 0;
		for (int i = 0; i < 6; i++) {
			t = t + a[i] * Math.pow(x, i + 1);
		}
		return 1 - Math.pow(1 + t, -16);
	}

	/**
	 * 正态分布函数值
	 */
	public static double Ni(double x) {
		return x == 0 ? 0.5 : (x > 0 ? Fi_erf_6(x) : 1 - Fi_erf_6(x));
	}

	private static void printTable() {// 调试，输出标准正态分布表
		for (double i = 0; i < 3; i += 0.1) {
			for (double j = 0; j < 0.1; j += 0.01) {
				if (i == 0) {
					if (j == 0)
						System.out.print("     " + j + "                ");
					else if (j <= 0.09)
						System.out.print((double) Math.round(j * 100) / 100 + "                ");
					else
						System.out.print("  ");
				}
			}
			if (i == 0) {
				System.out.println();
				System.out
						.print("     -------------------------------------------------------"
								+ "--------------------------------------------------------------------------"
								+ "--------------------------------------------------------------------------");
			}
			System.out.println("   |");
			System.out.print((double) Math.round(i * 10) / 10 + "| ");
			for (double j = 0; j < 0.1; j += 0.01) {
				if (i == 0 && j == 0)
					System.out.print(Ni(i + j) + "                ");
				else if (j <= 0.09)
					System.out.print(Ni(i + j) + " ");
				else
					System.out.print("  ");
			}
			System.out.println();
		}
	}

	public static void main(String[] args) {

		printTable();
	}
}
