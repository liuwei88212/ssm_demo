package com.lw.ssm.test.math.cal;

/**
 * Java 计算数学表达式（字符串解析求值工具）
 * 
 * http://www.cnblogs.com/woider/p/5331391.html
 * 
 * @author Administrator
 *
 */
public class MathTest {
	public static void main(String[] args) {
		String expression = "(0*1--3)-5/-4-(3*(-2.13))";
		double result = Calculator.conversion(expression);
		System.out.println(expression + " = " + result);
		System.out.println();
		
		
		System.out.println(Math.expm1(1));
	}
}
