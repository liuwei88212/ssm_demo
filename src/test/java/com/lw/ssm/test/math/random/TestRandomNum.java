package com.lw.ssm.test.math.random;

import java.util.Random;

/**
 * 1、带种子与不带种子的区别Random类使用的根本是策略分带种子和不带种子的Random的实例。
 * 通俗说，两者的区别是：带种子的，每次运行生成的结果都是一样的。 不带种子的，每次运行生成的都是随机的，没有规律可言。
 * 
 * @author Administrator
 *
 */
public class TestRandomNum {
	public static void main(String[] args) {
		randomTest();
		testNoSeed();
		testSeed1();
		testSeed2();
	}

	public static void randomTest() {
		System.out.println("--------------test()--------------");
		// 返回以毫秒为单位的当前时间。
		long r1 = System.currentTimeMillis();
		// 返回带正号的 double 值，大于或等于 0.0，小于 1.0。
		double r2 = Math.random();
		// 通过Random类来获取下一个随机的整数
		int r3 = new Random().nextInt();
		System.out.println("r1 = " + r1);
		System.out.println("r3 = " + r2);
		System.out.println("r2 = " + r3);
	}

	public static void testNoSeed() {
		System.out.println("--------------testNoSeed1()--------------");
		// 创建不带种子的测试Random对象
		Random random = new Random();
		for (int i = 0; i < 3; i++) {
			System.out.println(random.nextInt());
		}
	}

	public static void testSeed1() {
		System.out.println("--------------testSeed2()--------------");
		// 创建带种子的测试Random对象
		Random random = new Random(555L);
		for (int i = 0; i < 3; i++) {
			System.out.println(random.nextInt());
		}
	}

	public static void testSeed2() {
		System.out.println("--------------testSeed3()--------------");
		// 创建带种子的测试Random对象
		Random random = new Random();
		random.setSeed(555L);
		for (int i = 0; i < 3; i++) {
			System.out.println(random.nextInt());
		}
	}
}
