package com.lw.ssm.test.math.spark;

import java.util.Arrays;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaDoubleRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.mllib.linalg.Vector;
import org.apache.spark.mllib.linalg.Vectors;
import org.apache.spark.mllib.stat.Statistics;

/**
 * Correlations，相关度量，
 * 目前Spark支持两种相关性系数：皮尔逊相关系数（pearson）和斯皮尔曼等级相关系数（spearman）。
 * 
 * 相关系数是用以反映变量之间相关关系密切程度的统计指标。
 * 
 * 简单的来说就是相关系数绝对值越大（值越接近1或者-1）则表示数据越可进行线性拟合。
 * 
 * @author Administrator
 *
 */
public class Mllib_Demo4 {
	public static void main(String[] args) {

		System.setProperty("hadoop.home.dir", "D:/tools/hadoop-2.5.2");

		SparkConf sparkConf = new SparkConf().setAppName("Statistics").setMaster("local[2]");
		JavaSparkContext sc = new JavaSparkContext(sparkConf);
		JavaRDD<String> data = sc.textFile("/java/spark/statistics.txt");
		JavaRDD<Vector> parsedData = data.map(s -> {
			double[] values = Arrays.asList(s.split(" ")).stream()
					.mapToDouble(Double::parseDouble).toArray();
			return Vectors.dense(values);
		});

		JavaDoubleRDD seriesX = parsedData.mapToDouble(vector -> vector.apply(0));
		JavaDoubleRDD seriesY = parsedData.mapToDouble(vector -> vector.apply(1));

		System.out.println("pearson=" + Statistics.corr(seriesX.srdd(), seriesY.srdd(), "pearson"));
		System.out.println("spearman=" +Statistics.corr(seriesX.srdd(), seriesY.srdd(), "spearman"));
		
		System.out.println("-------------------------------------------");
		System.out.println(Statistics.corr(parsedData.rdd(), "pearson"));
		System.out.println("-------------------------------------------");
		System.out.println(Statistics.corr(parsedData.rdd(), "spearman"));
	}
}
