package com.lw.ssm.test.queue;

import java.util.List;

import org.slf4j.MDC;

import com.lw.ssm.common.util.BinaryRedisUtil;
import com.lw.ssm.common.util.SerializeUtil;
import com.lw.ssm.junit.bean.Teacher;

/**
 * Redis 消息队列测试
 * 
 * @author Administrator
 * 
 */
public class RedisQueneTest {
	public static byte[] redisKey;

	public static byte[] redisKeyDesc;
	
	static {

		// log4j 日志
		MDC.put("UserName", "liuwei");
		MDC.put("opttype", "JUnit");

		redisKey = "teacher_list".getBytes();
		
		redisKeyDesc = "teacher_list_desc".getBytes();

		init();
	}

	public static void main(String[] args) {
		
		int i=1;
		while (true) {
			
//			 brpop();

//			rpop();
			
			
			
			brpoplpush();
			
			try {
				if(i==1){
					throw new Exception("模拟异常");
				}
			} catch (Exception e) {
			}
		}

	}

	private static void rpop() {
		System.out.println("redis: rpop()");
		byte[] bytes = BinaryRedisUtil.rpop(redisKey);
		Teacher t = (Teacher) SerializeUtil.unserialize(bytes);
		if (t != null) {
			System.out.println(t.getUsername());
		}
	}

	private static void brpop() {
		List<byte[]> list = BinaryRedisUtil.brpop(redisKey, 0);
		print(list);
	}
	
	private static void brpoplpush() {
		byte[] value = BinaryRedisUtil.brpoplpush(redisKey, redisKeyDesc);
		
		Teacher t = (Teacher) SerializeUtil.unserialize(value);
		if (t != null) {
			System.out.println("->" + t.getUsername());
		}
	}
	
	private static void print(List<byte[]> list){
		if (list.size() == 2) {
			byte[] key = list.get(0);
			byte[] value = list.get(1);

			// System.out.println(key + ":" + value);

			Teacher t = (Teacher) SerializeUtil.unserialize(value);
			if (t != null) {
				System.out.println("->" + t.getUsername());
			}
		}
	}

	private static void init() {
		Teacher t1 = new Teacher("张三");
		BinaryRedisUtil.lpush(redisKey, SerializeUtil.serialize(t1));
		Teacher t2 = new Teacher("李四");
		BinaryRedisUtil.lpush(redisKey, SerializeUtil.serialize(t2));
		Teacher t3 = new Teacher("王五");
		BinaryRedisUtil.lpush(redisKey, SerializeUtil.serialize(t3));
	}
}
