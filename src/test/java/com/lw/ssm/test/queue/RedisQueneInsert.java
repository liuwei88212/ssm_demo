package com.lw.ssm.test.queue;

import com.lw.ssm.common.util.BinaryRedisUtil;
import com.lw.ssm.common.util.SerializeUtil;
import com.lw.ssm.junit.bean.Teacher;

public class RedisQueneInsert {
	
	public static byte[] redisKey ;
	
	static{
		redisKey = "teacher_list".getBytes();
	}
	
	public static void main(String[] args) {
		Teacher t1 = new Teacher("张三2");
		BinaryRedisUtil.lpush(redisKey, SerializeUtil.serialize(t1));
		Teacher t2 = new Teacher("李四2");
		BinaryRedisUtil.lpush(redisKey, SerializeUtil.serialize(t2));
		Teacher t3 = new Teacher("王五2");
		BinaryRedisUtil.lpush(redisKey, SerializeUtil.serialize(t3));
	}
}
