
package com.lw.ssm.test;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class BigDecimalTest
{
    public static void main(String[] args)
    {
        BigDecimal d1 = new BigDecimal("0.1");
        BigDecimal d2 = new BigDecimal("9");

        BigDecimal d3 = d1.divide(d2, 0, RoundingMode.HALF_UP);
        System.out.println(d3);

        System.out.println(d1.compareTo(d2));
        
        
        BigDecimal d4 = d1.multiply(d2);
        d4 = d4.setScale(0, RoundingMode.HALF_UP);
        System.out.println(d4);
        
        System.out.println("四舍五入取整:(0.9)=" + new BigDecimal("0.9").setScale(0, BigDecimal.ROUND_HALF_UP));
        
        BigDecimal goodsAmount = new BigDecimal(0);
        goodsAmount.add(new BigDecimal(d1.doubleValue()*2));
        

        //        System.out.println(d1.comp 
        //        BigDecimal d3 = null;

        //        System.out.println(d1.subtract(d3));

        //        System.out.println(d1.equals(d2));

        //        BigDecimal d4 = new BigDecimal(0.30d * 0.1d);
        //        System.out.println(d4.multiply(new BigDecimal(0.1)).doubleValue());

        //        System.out.println(d4+", "+BigDecimalUtil.giveYou(d4));

        //        System.out.println(new BigDecimal((0.03 * 0.1)+""));
        //        
        //        System.out.println(new BigDecimal(1000/10));
        //        
        //        
        //        System.out.println(d1.equals(d2));
    }
}
