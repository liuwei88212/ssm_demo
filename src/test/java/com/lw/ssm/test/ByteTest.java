
package com.lw.ssm.test;

import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;


public class ByteTest
{
    public static void main(String[] args) throws UnsupportedEncodingException
    {
        String s = "推广有奖";

        byte[] bytes = s.getBytes(Charset.forName("UTF8"));
        for (byte b : bytes)
        {
            String hex = Integer.toHexString(b & 0xFF);

            System.out.print(hex + " ");
        }

        System.out.println(new String(bytes, "utf-8"));

        System.out.println(toStringHex1("e68ea8e5b9bfe69c89e5a596"));
        System.out.println(toStringHex2("e68ea8e5b9bfe69c89e5a596"));
    }

    //转化字符串为十六进制编码  
    public static String toHexString(String s)
    {
        String str = "";
        for (int i = 0; i < s.length(); i++)
        {
            int ch = (int) s.charAt(i);
            String s4 = Integer.toHexString(ch);
            str = str + s4;
        }
        return str;
    }

    // 转化十六进制编码为字符串  
    public static String toStringHex1(String s)
    {
        byte[] baKeyword = new byte[s.length() / 2];
        for (int i = 0; i < baKeyword.length; i++)
        {
            try
            {
                baKeyword[i] = (byte) (0xff & Integer.parseInt(s.substring(i * 2, i * 2 + 2), 16));
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
        }
        try
        {
            s = new String(baKeyword, "utf-8");// UTF-16le:Not  
        }
        catch (Exception e1)
        {
            e1.printStackTrace();
        }
        return s;
    }

    // 转化十六进制编码为字符串  
    public static String toStringHex2(String s)
    {
        byte[] baKeyword = new byte[s.length() / 2];
        for (int i = 0; i < baKeyword.length; i++)
        {
            try
            {
                baKeyword[i] = (byte) (0xff & Integer.parseInt(s.substring(i * 2, i * 2 + 2), 16));
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
        }
        try
        {
            s = new String(baKeyword, "utf-8");// UTF-16le:Not  
        }
        catch (Exception e1)
        {
            e1.printStackTrace();
        }
        return s;
    }

}
