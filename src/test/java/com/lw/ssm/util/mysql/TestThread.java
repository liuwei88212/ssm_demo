package com.lw.ssm.util.mysql;

import java.util.concurrent.CountDownLatch;

public class TestThread  extends Thread{

	private CountDownLatch threadsSignal;
	
	public TestThread(CountDownLatch threadsSignal){
		this.threadsSignal = threadsSignal;
	}
	
	public void run() {
		System.out.println(Thread.currentThread().getName() + "开始...");
		try {
			Thread.sleep(10000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		threadsSignal.countDown();//线程结束时计数器减1
		System.out.println(Thread.currentThread().getName() + "结束. 还有" + threadsSignal.getCount() + " 个线程");
	}
}
