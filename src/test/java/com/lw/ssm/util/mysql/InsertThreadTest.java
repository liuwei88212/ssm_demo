package com.lw.ssm.util.mysql;

import java.util.concurrent.CountDownLatch;

public class InsertThreadTest {
	public static void main(String[] args) throws InterruptedException {
		
		long tStart = System.currentTimeMillis();
		System.out.println(Thread.currentThread().getName() + "开始");//打印开始标记
		
		int threadNum = 10;
		CountDownLatch threadSignal = new CountDownLatch(threadNum);// 初始化countDown

		for (int i = 1; i <= threadNum; i++) {
			new InsertThread(threadSignal).start();
			//new TestThread(threadSignal).start();
		}
		
		threadSignal.await();//等待所有子线程执行完
		
		System.out.println(Thread.currentThread().getName() + "结束.");//打印结束标记
		
		long tEnd = System.currentTimeMillis();
		System.out.println("总共用时:"+ (tEnd - tStart)/1000 + "s");
	}
}
