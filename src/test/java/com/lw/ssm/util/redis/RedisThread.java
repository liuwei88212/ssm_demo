package com.lw.ssm.util.redis;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.CountDownLatch;

import com.alibaba.fastjson.JSON;
import com.lw.ssm.common.util.RedisUtil;
import com.lw.ssm.common.util.UUIDUtils;
import com.lw.ssm.junit.bean.Teacher;
import com.lw.ssm.util.DateUtils;

public class RedisThread extends Thread {

	private CountDownLatch threadsSignal;

	private String key;

	public RedisThread(CountDownLatch threadsSignal, String redisKey) {
		this.threadsSignal = threadsSignal;
		this.key = redisKey;
	}

	public void run() {
		int count = 100000;

		// 开始时间
		Long begin = new Date().getTime();


		for (int i = 0; i < 10; i++) {
			List<String> list = new ArrayList<String>();
			
			// 第j次提交步长
			for (int j = 1; j <= count/10; j++) {

				Teacher t = new Teacher();
				t.setUuid(UUIDUtils.getUUID16());
				t.setUsername("1234567");
				t.setSex("男");
				t.setDesc("教师");
				t.setPicUrl("www.baidu.com");
				t.setSchoolName("XX大学");
				t.setDate(DateUtils.formatDateToString(new Date(),
						"yyyy-MM-dd HH:mm:ss"));
				t.setRemark("备注");

				String json = JSON.toJSONString(t);
				list.add(json);
			}
			
			System.out.println("执行rpush: " + RedisUtil.rpush(key, list, 0));
		}

		threadsSignal.countDown();// 线程结束时计数器减1
		System.out.println(Thread.currentThread().getName() + "结束. 还有"+ threadsSignal.getCount() + " 个线程");

		// 结束时间
		Long end = new Date().getTime();
		// 耗时
		System.out.println(count + "条数据插入花费时间 : " + (end - begin) / 1000 + " s");
		System.out.println("插入完成");
	}
}
