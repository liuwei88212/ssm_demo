package com.lw.ssm.util.redis;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import redis.clients.jedis.Jedis;

import com.alibaba.fastjson.JSON;
import com.lw.ssm.common.util.RedisUtil;
import com.lw.ssm.common.util.UUIDUtils;
import com.lw.ssm.junit.bean.Teacher;
import com.lw.ssm.util.DateUtils;

public class RedisTest {
	public static void main(String[] args) {
		Jedis jedis = RedisUtil.getJedis();
		System.out.println("redis连接："+jedis.isConnected());
		
		RedisUtil.set("123".getBytes(), "123".getBytes());

		//System.out.println(createJedis("192.168.8.9", 6379, "zxb123").isConnected());
		
		
		String key1 = "teacher1"; //Rpush: 1000W
//		String key2 = "teacher2"; //Sadd：1000W
//		String key3 = "teacher3"; //Zadd：1400000
//		String key4 = "teacher4"; //
//		
		insert(key1);
		
		//System.out.println(RedisUtil.llen(key));
	
	}

	public static Jedis createJedis(String host, int port, String passwrod) {
		Jedis jedis = new Jedis(host, port);

		jedis.auth(passwrod);

		return jedis;
	}
	
	
	public static void insert(String key ) {
		// 开始时间
		Long begin = new Date().getTime();
		
        // 外层循环，总提交事务次数
        for (int i = 1; i <= 10; i++) {
        	
        	List<String> list = new ArrayList<String>();
        	Map<String, Double> map = new HashMap<String, Double>();
        	Map<String,String> hmap = new HashMap<String, String>();
        	
            // 第j次提交步长
            for (int j = 1; j <= 10000; j++) {
            	
            	Teacher t = new Teacher();
            	t.setUuid(UUIDUtils.getUUID16());
            	t.setUsername("1234567");
            	t.setSex("男");
            	t.setDesc("教师");
            	t.setPicUrl("www.baidu.com");
            	t.setSchoolName("XX大学");
            	t.setDate(DateUtils.formatDateToString(new Date(), "yyyy-MM-dd HH:mm:ss"));
            	t.setRemark("备注");
            	
            	String json = JSON.toJSONString(t);
            	//System.out.println("json: "+json);
            	
            	list.add(json);
//            	map.put(json, new Double(i*j));
//            	hmap.put("index_"+i*j, json);
            }
            
            System.out.println("当前循环："+ i+", 执行rpush: "+RedisUtil.rpush(key, list, 0)); 
//            System.out.println("当前循环："+ i+", 执行sadd: "+RedisUtil.sadd(key, list, 0)); 
//            System.out.println("当前循环："+ i+",  执行zadd: "+RedisUtil.zadd(key, map, 0));
//            System.out.println("hmset: "+i+"->"+RedisUtil.hmset(key, hmap, 0));
            
            list.clear();
//            map.clear();
//            hmap.clear();
        }
		
		// 结束时间
		Long end = new Date().getTime();
		// 耗时
		System.out.println("1000万条数据插入花费时间 : " + (end - begin) / 1000 + " s");
		System.out.println("插入完成");
	}
}
