package com.lw.ssm.util.mongo;

import java.util.concurrent.CountDownLatch;

import org.bson.Document;

import com.mongodb.client.MongoCollection;

public class MongoThreadTest {
	public static void main(String[] args) throws InterruptedException {
		
		MongoDBUtil mongoDBUtil = new MongoDBUtil(MongoDBUtil.db_gbc);
		MongoCollection<Document> collection = mongoDBUtil.getCollection("t_teacher_2");
		
		long tStart = System.currentTimeMillis();
		System.out.println(Thread.currentThread().getName() + "开始");//打印开始标记
		
		int threadNum = 100;
		CountDownLatch threadSignal = new CountDownLatch(threadNum);// 初始化countDown

		for (int i = 1; i <= threadNum; i++) {
			new MongoThread(threadSignal, collection).start();
		}
		
		threadSignal.await();//等待所有子线程执行完
		
		System.out.println(Thread.currentThread().getName() + "结束.");//打印结束标记
		
		long tEnd = System.currentTimeMillis();
		System.out.println("总共用时:"+ (tEnd - tStart)/1000 + "s");
	}
}
