package com.lw.ssm.util.mongo;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.configuration.CompositeConfiguration;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.bson.Document;
import org.bson.conversions.Bson;
import org.bson.types.ObjectId;

import com.alibaba.fastjson.JSONObject;
import com.lw.ssm.junit.bean.Teacher;
import com.mongodb.BasicDBObject;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientOptions;
import com.mongodb.MongoCredential;
import com.mongodb.ServerAddress;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.MongoIterable;
import com.mongodb.client.model.Filters;
import com.mongodb.client.result.DeleteResult;

/**
 * MongoDB工具类 Mongo实例代表了一个数据库连接池，即使在多线程的环境中，一个Mongo实例对我们来说已经足够了<br>
 * 注意Mongo已经实现了连接池，并且是线程安全的。 <br>
 * 设计为单例模式， 因 MongoDB的Java驱动是线程安全的，对于一般的应用，只要一个Mongo实例即可，<br>
 * Mongo有个内置的连接池（默认为10个） 对于有大量写和读的环境中，为了确保在一个Session中使用同一个DB时，<br>
 * DB和DBCollection是绝对线程安全的<br>
 * 
 * @author zhoulingfei
 * @date 2015-5-29 上午11:49:49
 * @version 0.0.0
 * @Copyright (c)1997-2015 NavInfo Co.Ltd. All Rights Reserved.
 * @see http://www.cnblogs.com/zhoulf/p/4571647.html
 */
public class MongoDBUtil {

	private static MongoClient mongoClient;

	private static String ip;

	private static int port;

	private static String username;

	private static String password;

	private static boolean auth;

	private String dbName;

	public static String db_gbc = "gdc";

	public static String db_smc = "smc";

	static {
		CompositeConfiguration config = new CompositeConfiguration();
		try {
			config.addConfiguration(new PropertiesConfiguration(
					"conf/mongo.properties"));
		} catch (ConfigurationException e) {
			e.printStackTrace();
		}
		// 从配置文件中获取属性值
		ip = config.getString("mongo.host");
		port = config.getInt("mongo.port");

		auth = config.getBoolean("mongo.auth");
		username = config.getString("mongo.username");
		password = config.getString("mongo.password");

		System.out.println(ip + "," + port + "," + username + "," + password);
	}

	public MongoDBUtil(String dbName) {
		this.dbName = dbName;

		MongoClientOptions.Builder buide = new MongoClientOptions.Builder();
		buide.connectionsPerHost(200);// 与目标数据库可以建立的最大链接数
		buide.connectTimeout(1000 * 60 * 20);// 与数据库建立链接的超时时间
		buide.maxWaitTime(100 * 60 * 5);// 一个线程成功获取到一个可用数据库之前的最大等待时间
		buide.threadsAllowedToBlockForConnectionMultiplier(100);
		buide.maxConnectionIdleTime(0);
		buide.maxConnectionLifeTime(0);
		buide.socketTimeout(0);
		buide.socketKeepAlive(true);
		MongoClientOptions myBuild = buide.build();

		if (!auth) {
			mongoClient = new MongoClient(ip, port);
		} else {
			MongoCredential credential = MongoCredential.createCredential(
					username, dbName, password.toCharArray());
			mongoClient = new MongoClient(new ServerAddress(ip, port),
					Arrays.asList(credential), myBuild);
		}

		// or, to connect to a replica set, with auto-discovery of the primary,
		// supply a seed list of members
		// List<ServerAddress> listHost = Arrays.asList(new
		// ServerAddress("localhost", 27017), new ServerAddress("localhost",
		// 27018));
		// instance.mongoClient = new MongoClient(listHost);

		System.out.println(mongoClient);
	}

	// ------------------------------------共用方法---------------------------------------------------
	/**
	 * 获取DB实例 - 指定DB
	 * 
	 * @param dbName
	 * @return
	 */
	public MongoDatabase getDB() {
		MongoDatabase database = mongoClient.getDatabase(dbName);
		return database;
	}

	/**
	 * 获取collection对象 - 指定Collection
	 * 
	 * @param collName
	 * @return
	 */
	public MongoCollection<Document> getCollection(String collName) {
		if (null == collName || "".equals(collName)) {
			return null;
		}
		if (null == dbName || "".equals(dbName)) {
			return null;
		}
		MongoCollection<Document> collection = getDB().getCollection(collName);
		return collection;
	}

	/**
	 * 查询DB下的所有表名
	 */
	public List<String> getAllCollections() {
		MongoIterable<String> colls = getDB().listCollectionNames();
		List<String> _list = new ArrayList<String>();
		for (String s : colls) {
			_list.add(s);
		}
		return _list;
	}

	/**
	 * 获取所有数据库名称列表
	 * 
	 * @return
	 */
	public MongoIterable<String> getAllDBNames() {
		MongoIterable<String> s = mongoClient.listDatabaseNames();
		return s;
	}

	/**
	 * 删除一个数据库
	 */
	public void dropDB() {
		getDB().drop();
	}

	public void insertAll(String collName, List<Document> docList) {
		getCollection(collName).insertMany(docList);
	}

	/**
	 * 查找对象 - 根据主键_id
	 * 
	 * @param collection
	 * @param id
	 * @return
	 */
	public Document findById(MongoCollection<Document> coll, String id) {
		ObjectId _idobj = null;
		try {
			_idobj = new ObjectId(id);
		} catch (Exception e) {
			return null;
		}
		Document myDoc = coll.find(Filters.eq("_id", _idobj)).first();
		return myDoc;
	}

	/** 统计数 */
	public int getCount(MongoCollection<Document> coll) {
		int count = (int) coll.count();
		return count;
	}

	/** 条件查询 */
	public MongoCursor<Document> find(MongoCollection<Document> coll,
			Bson filter) {
		return coll.find(filter).iterator();
	}

	/** 分页查询 */
	public MongoCursor<Document> findByPage(MongoCollection<Document> coll,
			Bson filter, int pageNo, int pageSize) {
		Bson orderBy = new BasicDBObject("_id", 1);
		return coll.find(filter).sort(orderBy).skip((pageNo - 1) * pageSize)
				.limit(pageSize).iterator();
	}

	/**
	 * 通过ID删除
	 * 
	 * @param coll
	 * @param id
	 * @return
	 */
	public int deleteById(MongoCollection<Document> coll, String id) {
		int count = 0;
		ObjectId _id = null;
		try {
			_id = new ObjectId(id);
		} catch (Exception e) {
			return 0;
		}
		Bson filter = Filters.eq("_id", _id);
		DeleteResult deleteResult = coll.deleteOne(filter);
		count = (int) deleteResult.getDeletedCount();
		return count;
	}

	/**
	 * FIXME
	 * 
	 * @param coll
	 * @param id
	 * @param newdoc
	 * @return
	 */
	public Document updateById(MongoCollection<Document> coll, String id,
			Document newdoc) {
		ObjectId _idobj = null;
		try {
			_idobj = new ObjectId(id);
		} catch (Exception e) {
			return null;
		}
		Bson filter = Filters.eq("_id", _idobj);
		// coll.replaceOne(filter, newdoc); // 完全替代
		coll.updateOne(filter, new Document("$set", newdoc));
		return newdoc;
	}

	public void dropCollection(String collName) {
		getDB().getCollection(collName).drop();
	}

	/**
	 * 关闭Mongodb
	 */
	public void close() {
		if (mongoClient != null) {
			mongoClient.close();
			mongoClient = null;
		}
	}

	/**
	 * 测试入口
	 * 
	 * @param args
	 */
	public static void main(String[] args) {

		String collName = "t_teacher_1";

		MongoDBUtil mongoDBUtil = new MongoDBUtil(MongoDBUtil.db_gbc);

		MongoCollection<Document> collection = mongoDBUtil
				.getCollection(collName);

		Bson filter = new BasicDBObject();
		MongoCursor<Document> cursor = mongoDBUtil.findByPage(collection,filter, 1, 10);
		while (cursor.hasNext()) {
			
			String json = cursor.next().toJson();
			
			Teacher teacher = JSONObject.parseObject(json, Teacher.class);
			System.out.println(JSONObject.toJSONString(teacher));
		}

		// 插入多条
		// for (int i = 1; i <= 4; i++) {
		// Document doc = new Document();
		// doc.put("name", "zhoulf");
		// doc.put("school", "NEFU" + i);
		// Document interests = new Document();
		// interests.put("game", "game" + i);
		// interests.put("ball", "ball" + i);
		// doc.put("interests", interests);
		// coll.insertOne(doc);
		// }

		// // 根据ID查询
		// String id = "556925f34711371df0ddfd4b";
		// Document doc = MongoDBUtil2.instance.findById(coll, id);
		// System.out.println(doc);

		// 查询多个
		// MongoCursor<Document> cursor1 = coll.find(Filters.eq("name",
		// "zhoulf")).iterator();
		// while (cursor1.hasNext()) {
		// org.bson.Document _doc = (Document) cursor1.next();
		// System.out.println(_doc.toString());
		// }
		// cursor1.close();

		// 查询多个
		// MongoCursor<Person> cursor2 = coll.find(Person.class).iterator();

		// 删除数据库
		// MongoDBUtil2.instance.dropDB("testdb");

		// 删除表
		// MongoDBUtil2.instance.dropCollection(dbName, collName);

		// 修改数据
		// String id = "556949504711371c60601b5a";
		// Document newdoc = new Document();
		// newdoc.put("name", "时候");
		// MongoDBUtil.instance.updateById(coll, id, newdoc);

		// 统计表
		// System.out.println(MongoDBUtil.instance.getCount(coll));

		// 查询所有
		// Bson filter = Filters.eq("count", 0);
		// MongoDBUtil.instance.find(coll, filter);

	}
}
