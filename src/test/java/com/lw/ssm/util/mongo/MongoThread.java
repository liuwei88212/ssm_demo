package com.lw.ssm.util.mongo;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.CountDownLatch;

import org.bson.Document;

import com.lw.ssm.common.util.UUIDUtils;
import com.mongodb.client.MongoCollection;

public class MongoThread extends Thread {
	
	private CountDownLatch threadsSignal;
	
	private MongoCollection<Document> collection ;
	
	public MongoThread(CountDownLatch threadsSignal, MongoCollection<Document> collection ){
		this.threadsSignal = threadsSignal;
		this.collection = collection;
	}
	
	public void run() {
		int count = 100000;
		
		// 开始时间
		Long begin = new Date().getTime();
		
    	List<Document> list = new ArrayList<Document>();
        // 第j次提交步长
        for (int j = 1; j <= count; j++) {
        	
        	Document doc = new Document();
        	
        	doc.put("uuid", UUIDUtils.getUUID16());
        	doc.put("username","123456");
        	doc.put("sex", "男");
        	doc.put("desc", "教师");
        	doc.put("picUrl", "www.baidu.com");
        	doc.put("schoolName", "XX大学");
        	doc.put("registTime", "2016-10-22 10:00:00");
        	doc.put("remark", "备注");
        	
            list.add(doc);
                
            collection.insertMany(list);
            list.clear();
        }
        
        threadsSignal.countDown();//线程结束时计数器减1
		System.out.println(Thread.currentThread().getName() + "结束. 还有" + threadsSignal.getCount() + " 个线程");
		
		// 结束时间
		Long end = new Date().getTime();
		// 耗时
		System.out.println(count+"条数据插入花费时间 : " + (end - begin) / 1000 + " s");
		System.out.println("插入完成");
		
	}
}
