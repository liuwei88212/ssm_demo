package com.lw.ssm.util.mongo;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.bson.Document;

import com.lw.ssm.common.util.UUIDUtils;
import com.mongodb.client.MongoCollection;

public class MongoTest {

	public static void main(String[] args) throws ClassNotFoundException,
			SQLException {
		MongoDBUtil mongoDBUtil = new MongoDBUtil(MongoDBUtil.db_gbc);
		MongoCollection<Document> collection = mongoDBUtil.getCollection("t_teacher_3");
		insert(collection);
		mongoDBUtil.close();
		
	}

	public static void insert(MongoCollection<Document> collection ) {
		// 开始时间
		Long begin = new Date().getTime();
		
        // 外层循环，总提交事务次数
        for (int i = 1; i <= 1000; i++) {
        	
        	List<Document> list = new ArrayList<Document>();
            // 第j次提交步长
            for (int j = 1; j <= 10000; j++) {
            	
            	Document doc = new Document();
            	
            	doc.put("uuid", UUIDUtils.getUUID16());
            	doc.put("username","123456");
            	doc.put("sex", "男");
            	doc.put("desc", "教师");
            	doc.put("picUrl", "www.baidu.com");
            	doc.put("schoolName", "XX大学");
            	doc.put("registTime", "2016-10-22 10:00:00");
            	doc.put("remark", "备注");
            	
                list.add(doc);
                
//                collection.insertOne(doc);
            }
            
            collection.insertMany(list);
            list.clear();
            
            System.out.println("========执行次数："+i+"=========");
        }
		
		// 结束时间
		Long end = new Date().getTime();
		// 耗时
		System.out.println("1000万条数据插入花费时间 : " + (end - begin) / 1000 + " s");
		System.out.println("插入完成");
	}
}
