package com.lw.ssm.util;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;
import redis.clients.jedis.exceptions.JedisConnectionException;

/**
 * 类名称：RedisCache 类描述：使用第三方缓存服务器redis，处理二级缓存 修改备注：
 * 
 * @version V1.0
 * 
 */
public class CachePool {
	JedisPool pool;
	private static final CachePool cachePool = new CachePool();

	public static CachePool getInstance() {
		return cachePool;
	}

	private CachePool() {
		JedisPoolConfig config = new JedisPoolConfig();
		PropertiesLoader pl = new PropertiesLoader(
				"classpath:conf/redis.properties");
		// 最大空闲连接数, 默认8个
		config.setMaxIdle(Integer.parseInt(pl.getProperty("maxIdle")));
		// 获取连接时的最大等待毫秒数(如果设置为阻塞时BlockWhenExhausted),如果超时就抛异常, 小于零:阻塞不确定的时间,
		// 默认-1
		config.setMaxWaitMillis(Integer.parseInt(pl
				.getProperty("maxWaitMillis")));
		pool = new JedisPool(config, pl.getProperty("redisvip"));
	}

	public Jedis getJedis() {
		Jedis jedis = null;
		boolean borrowOrOprSuccess = true;
		try {
			jedis = pool.getResource();
		} catch (JedisConnectionException e) {
			borrowOrOprSuccess = false;
			if (jedis != null)
				pool.returnBrokenResource(jedis);
		} finally {
			if (borrowOrOprSuccess)
				pool.returnResource(jedis);
		}
		jedis = pool.getResource();
		return jedis;
	}

	public JedisPool getJedisPool() {
		return this.pool;
	}

}
