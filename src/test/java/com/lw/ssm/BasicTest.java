package com.lw.ssm;  

import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
            "classpath:spring/spring.xml",
            "classpath:spring/spring-dubbo-provider.xml",
//            "classpath:spring/spring-dubbo-consumer.xml",
            
//            "classpath:spring/spring-ehcache.xml",
            "classpath:spring/spring-mybatis.xml"})
public class BasicTest {
	
}
