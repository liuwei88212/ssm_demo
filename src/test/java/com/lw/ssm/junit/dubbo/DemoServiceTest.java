package com.lw.ssm.junit.dubbo;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.lw.ssm.BasicTest;
import com.lw.ssm.dubbo.DemoService;

public class DemoServiceTest extends BasicTest{

	
	@Autowired
	private DemoService demoService;
	
	@Test
	public void hello(){
		String str = demoService.hello("刘维");
		System.out.println(str);
	} 
}
