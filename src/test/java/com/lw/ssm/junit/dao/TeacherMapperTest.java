package com.lw.ssm.junit.dao;

import java.util.List;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.lw.ssm.BasicTest;
import com.lw.ssm.common.bean.Teacher;
import com.lw.ssm.common.bean.TeacherExample;
import com.lw.ssm.common.mapper.TeacherMapper;

public class TeacherMapperTest  extends BasicTest{

	@Autowired
	private TeacherMapper teacherMapper;
	
	@Test
	public void select(){
		
		TeacherExample example = new TeacherExample();
		example.setOffset(0);
		example.setPagesize(10);
		List<Teacher> list = teacherMapper.selectByExample(example);
		for(Teacher t: list){
			System.out.println(t.getUuid());
		}
	}
	
	
	@Test
	public void update(){
		
		Teacher t = teacherMapper.selectByPrimaryKey(1l);
		t.settName("111111");
		teacherMapper.updateByPrimaryKeySelective(t);
		
	}
}
